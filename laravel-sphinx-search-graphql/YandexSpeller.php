<?php
namespace App\Services;

use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Psr\Http\Message\ResponseInterface;

/**
 * Class YandexSpeller
 */
class YandexSpeller {

    protected const ENDPOINT = 'https://speller.yandex.net/services/spellservice.json/checkText';
    protected const LANG = 'ru,en';
    protected const OPTIONS = '0';
    protected const FORMAT = 'plain';

    /**
     * @param string $text
     * @return string|null
     */
    public static function replace(string $text): ?string
    {
        if($values = self::check($text)->toArray()){
            return str_replace(array_keys($values), $values, $text);
        }
        return null;
    }

    /**
     * @param string $text
     * @return Collection
     */
    public static function check(string $text): Collection
    {
        return self::getResponse(self::getClient()->post(self::ENDPOINT, self::getParams($text)));
    }

    /**
     * @param ResponseInterface $response
     * @return Collection
     */
    protected static function getResponse(ResponseInterface $response): Collection
    {
        return self::getCollection(json_decode($response->getBody()->getContents(), true));
    }

    /**
     * @param array $values
     * @return Collection
     */
    protected static function getCollection(array $values): Collection
    {
        return new Collection(array_combine(
            array_column($values, 'word'), array_map('current', array_column($values, 's'))
        ));
    }

    /**
     * @return Client
     */
    protected static function getClient(): Client
    {
        return new Client();
    }

    /**
     * @param string $text
     * @return array
     */
    protected static function getParams(string $text): array
    {
        return [
            'form_params' => [
                'text' => $text,
                'lang' => self::LANG,
                'options' => self::OPTIONS,
                'format' => self::FORMAT
            ]
        ];
    }

}