<?php

namespace App\GraphQL\Query;

use App\GraphQL\Error\ValidationError;
use Folklore\GraphQL\Support\Facades\GraphQL;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

use \App\Services\YandexSpeller;

/**
 * Class SearchQuery
 * @package App\GraphQL\Query
 */
class SearchQuery extends Query
{
    protected $attributes = [
        'name' => 'search',
        'description' => 'Обработчик поискового запроса.'
    ];

    /**
     * @return ObjectType
     */
    public function type(): ObjectType
    {
        return GraphQL::type('ProductSearchResultType');
    }

    /**
     * @return array
     */
    public function args(): array
    {
        return [
            'query' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Строка запроса. Не менее 3-х символов.',
            ],
            'storeId' => [
                'type' => Type::int(),
                'description' => 'Код магазина.',
            ],
            'orderViews' => [
                'type' => GraphQL::type('OrderEnum'),
                'description' => 'Порядок сортировки по популярности.',
            ],
            'orderPrice' => [
                'type' => GraphQL::type('OrderEnum'),
                'description' => 'Порядок сортировки по цене.',
            ],
            'priceMin' => [
                'type' => Type::float(),
                'description' => 'Минимальная цена.'
            ],
            'priceMax' => [
                'type' => Type::float(),
                'description' => 'Максимальная цена.'
            ],
            'brand' => [
                'type' => Type::string(),
                'description' => 'Бренд.'
            ]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @return array
     */
    public function resolve($root, $args): array
    {
        $validator = \Validator::make($args, [
            'query' => 'required|min:3',
            'storeId' => 'required|int',
            'priceMin' => 'int',
            'priceMax' => 'int'
        ], [
            'query.required' => 'Поле :attribute обязательно для заполнения.',
            'storeId.required' => 'Поле :attribute обязательно для заполнения.',
            'query.min' => 'Значение поля :attribute должно иметь не менее :min символов.',
            'storeId.int' => 'Значение поля :attribute должно быть целым числом.',
            'priceMin.int' => 'Значение поля :attribute должно быть числом.',
            'priceMax.int' => 'Значение поля :attribute должно быть числом.',
        ]);

        if ($validator->messages()->count()) {
            throw with(new ValidationError('Некорректно переданы аргументы.'))
                ->setValidator($validator);
        }

        $store_id = array_get($args, 'storeId');
        $orderViews = array_get($args, 'orderViews');
        $orderPrice = array_get($args, 'orderPrice');
        $brand = array_get($args, 'brand');
        $priceMin = array_get($args, 'priceMin');
        $priceMax = array_get($args, 'priceMax');
        $spelling = array_get($args, 'spelling');

        $query = $this->getQueryString(array_get($args, 'query'));
        $goods = $this->getGoods($spelling ?: $query, $store_id, $brand);

        if ($goods->isEmpty() && is_numeric($query)) {
            $goods = $this->getGoods($this->getQueryNumber($query), $store_id, $brand);
        }

        $goodsIds = $this->getGoodsIds($spelling ?: $query, $store_id);

        if (!$spelling && $spell = $this->getSpelling($query, $store_id, $goodsIds)) {
            return $this->resolve($root, $args + ['spelling' => $spell]);
        }

        $goodsItems = \FeedGoods::whereIds($goods->pluck('id'))
            ->wherePriceMin($priceMin)->wherePriceMax($priceMax)
            ->orderByViews($orderViews)->orderByPrice($orderPrice)
            ->withExistingInStores($store_id, $goods->pluck('city_id')->first())
            ->orderByIds($goods->pluck('id'));

        $categories = \Category::WhereGoodsIds($goodsItems->get(['feed_goods.id']))
            ->orderByGoodsIds($goods->pluck('id'));

        $synonym = $this->getSynonym($spelling ?: $query);

        return [
            'categories' => $this->getCategories($categories),
            'products' => $this->getProducts($goodsItems),
            'searchSummary' => $this->getSummary($spelling, $synonym->to ?? null, $goodsIds)
        ];
    }

    /**
     * @param string $query
     * @param int $store_id
     * @param int $count
     * @param string $spell
     * @param int $spell_count
     * @return \SearchLog
     */
    protected function setSearchLog(string $query, int $store_id, int $count, string $spell, int $spell_count): \SearchLog
    {
        return \SearchLog::create([
            'search_string' => $query,
            'store_id' => $store_id,
            'count' => $count,
            'spelling' => $spell,
            'count_after_spelling' => $spell_count,
            'device_id' => request()->header('device-id') ?? '',
            'os_id' => request()->header('os-id') ?? '',
            'version' => request()->header('version') ?? '',
            'login' => request()->header('login') ?? ''
        ]);
    }

    /**
     * @param string $query
     * @return null|\SearchSynonym
     */
    protected function getSynonym(string $query): ?\SearchSynonym
    {
        return \SearchSynonym::LikeFrom($query)->get()->first();
    }

    /**
     * @param Builder $categories
     * @return Collection
     */
    protected function getCategories(Builder $categories): Collection
    {
        return $categories->take(4)->get(['category.*']);
    }

    /**
     * @param Builder $goodsItems
     * @return Collection
     */
    protected function getProducts(Builder $goodsItems): Collection
    {
        return $goodsItems->take(20)
            ->withStore()->selectIsNew()->get([
                'feed_goods.*', 'stores.id as store_id'
            ])
            ->map(function (\FeedGoods $item): \FeedGoods {
                $item->{'image'} = explode(' ', $item->{'image'});
                return $item;
            });
    }

    /**
     * @param string $query
     * @param int $store_id
     * @return Collection
     */
    protected function getGoodsIds(string $query, int $store_id): Collection
    {
        return \SearchGoods::MatchName($query)
            ->whereStore($store_id)
            ->limit(1000)->get(['id']);
    }

    /**
     * @param string $query
     * @param int $store_id
     * @param string|null $brand
     * @return Collection
     */
    protected function getGoods(string $query, int $store_id, ?string $brand): Collection
    {
        return \SearchGoods::MatchName($query)
            ->whereStore($store_id)->whereManufacturer($brand)
            ->limit(1000)->get();
    }

    /**
     * @param null|string $spelling
     * @param null|string $synonym
     * @param Collection $goodsIds
     * @return array
     */
    protected function getSummary(?string $spelling, ?string $synonym, Collection $goodsIds): array
    {
        return [
            'spelling' => $spelling,
            'synonym' => $synonym,
            'brands' => $this->getBrands($goodsIds),
            'price_max' => $this->getPriceOrder($goodsIds, 'desc'),
            'price_min' => $this->getPriceOrder($goodsIds, 'asc')
        ];
    }

    /**
     * @param Collection $goodsIds
     * @return Collection
     */
    protected function getBrands(Collection $goodsIds): Collection
    {
        return \FeedGoods::whereIds($goodsIds)
            ->IsAvailable()->selectManufacturer()
            ->get(['manufacturer'])->pluck('manufacturer');
    }

    /**
     * @param Collection $goodsIds
     * @param string $order
     * @return string|null
     */
    protected function getPriceOrder(Collection $goodsIds, string $order): ?string
    {
        return \FeedGoods::whereIds($goodsIds)
            ->IsAvailable()->orderByPrice($order)
            ->take(1)->get(['price'])->pluck('price')->first();
    }

    /**
     * @return bool
     */
    protected function getSpellerExecution(): bool
    {
        return filter_var(\Setting::whereSpeller()->get()->pluck('text')->first(), FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Replace all symbols in query string
     * @param string $query
     * @return string
     */
    protected function getQueryString(string $query): string
    {
        return preg_replace('/[^\w]/miu', ' & ', $query);
    }

    /**
     * @param string $query
     * @return string
     */
    protected function getQueryNumber(string $query): string
    {
        return substr($query, 0, 6);
    }

    /**
     * @param string $query
     * @param int $store_id
     * @param Collection $goodsIds
     * @return null|string
     */
    protected function getSpelling(string $query, int $store_id, Collection $goodsIds): ?string
    {
        if ($goodsIds->count() || !$this->getSpellerExecution()) {
            $this->setSearchLog($query, $store_id, $goodsIds->count(), '', 0);
            return null;
        }

        try {
            if (!$text = YandexSpeller::replace($query)) {
                $this->setSearchLog($query, $store_id, 0, '', 0);
                return null;
            }
        } catch (RequestException $e) {
            $this->setSearchLog($query, $store_id, 0, 'Service error', 0);
            return null;
        }

        if (!$count = $this->getGoodsIds($text, $store_id)->count()) {
            $this->setSearchLog($query, $store_id, 0, $text, 0);
            return null;
        }

        $this->setSearchLog($query, $store_id, 0, $text, $count);
        return $text;
    }

}