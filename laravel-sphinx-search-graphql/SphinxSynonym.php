<?php

namespace App\Services;

/**
 * Class SphinxSynonyms
 * @package App\Services
 */
class SphinxSynonyms
{

    protected const STATIC_FILE_PATH = '/docker/sphinx/wordforms.txt';

    /**
     * @param string $from
     * @param string $to
     */
    public static function set(string $from, string $to): void
    {
        //@TODO: для интерфесай добавления синонимов из админки
    }

    /**
     * @param string $from
     * @return null|\stdClass
     */
    public static function get(string $from): ?\stdClass
    {
        $result = null;
        self::list(function (\stdClass $item) use ($from, &$result): void {
            if (strpos($item->from, $from) !== false) {
                $result = $item;
            }
        });
        return $result;
    }

    /**
     * @param callable $callback
     */
    public static function list(callable $callback): void {
        foreach (file(base_path() . self::STATIC_FILE_PATH) as $line) {
            $callback(self::getItem($line));
        }
    }

    /**
     * @param string $line
     * @return \stdClass
     */
    protected static function getItem(string $line): \stdClass
    {
        return (object) array_combine(
            ['from', 'to'],
            explode(' > ', str_replace(PHP_EOL, '', $line))
        );
    }
}