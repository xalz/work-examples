<?php

namespace  BgStripeImport\Includes;

class CSV
{
    protected $_file;

    public function __construct($file)
    {
        $this->_file = new \ArrayObject((array) $file, \ArrayObject::ARRAY_AS_PROPS);
    }

    public function parse()
    {
        $data = [];
        $file = file($this->_file->tmp_name);
        $items = array_map('str_getcsv', $file);
        $fields = $this->getFields($items);
        $values = $this->getValues($items);

        foreach ($values as $items) {
            $object = new CSVObject();
            foreach ($items as $i => $item) {
                $object->set($fields[$i], $item);
            }
            $data[] = $object;
        }
        return $data;
    }

    protected function getValues(array $items)
    {
        return array_filter($items, function($key) {
            return $key > 0;
        }, ARRAY_FILTER_USE_KEY);
    }

    protected function getFields(array $items)
    {
        return current($items);
    }
}

