<?php

namespace BgStripeImport\Includes;

use BgStripeImport\Helper\Logger;

class StripeImport
{
    const EXECUTION_LIMIT = 30000000;
    const MEMORY_LIMIT = '128M';
    const LIFETIME_PLAN_SKU = 'lifetime_old';

    private $_items;

    private $_logger;

    private $_errors = [];

    public function __construct(array $csv)
    {
        $this->_items = $csv;
        $this->_logger = new Logger();
    }

    private function getPlanSku(CSVObject $item)
    {
        $planSku = $item->get('Plan') ? $item->get('Plan') : self::LIFETIME_PLAN_SKU;
        return $planSku;
    }

    private function getAccessPlan(CSVObject $item)
    {
        $query = new \WP_Query([
            'post_type' => 'llms_access_plan',
            'meta_query' => [
                [
                    'key' => '_llms_sku',
                    'value' => $this->getPlanSku($item),
                    'compare' => 'LIKE',
                ],
            ]
        ]);
        $plan = current($query->posts);
        return $plan ? new \LLMS_Access_Plan($plan) : null;
    }

    private function getUserEmail(CSVObject $item)
    {
       $userEmail = null;
       if ($item->get('Customer Email')) {
           $userEmail = $item->get('Customer Email');
       } elseif ($item->Email) {
           $userEmail = $item->Email;
       }
       return $userEmail;
    }

    private function getUser(CSVObject $item)
    {
        $user = get_user_by('email',  $this->getUserEmail($item));
        if (!$user) {
            if ($this->getUserEmail($item)) {
                $userEmail = $this->getUserEmail($item);
                $this->_logger->logInfo("user {$userEmail} not found");
            } else {
                $this->_logger->logInfo("EMPTY USER COLUMN IN CSV");
            }
        }
        return $user ? new \LLMS_Student($user) : null;
    }

    private function getGetaway()
    {
        return LLMS()->payment_gateways()->get_gateway_by_id('stripe');
    }

    private function setError($type, CSVObject $item)
    {
        $item->set('type', $type);
        return $this->_errors[] = $item;
    }

    private function setOrder(CSVObject $item)
    {
        $order = new \LLMS_Order('new');
        $order->set('gateway_customer_id', $item->get('Customer ID'));
        $order->set('gateway_source_id', $item->get('id'));
        $order->set('date_trial_end', $item->get('Trial End (UTC)'));
        $order->set('post_date', $item->get('Start (UTC)'));
        $order->set('status', 'llms-active');
        $this->setDate($order->get('id'), $item->get('Created (UTC)'));
        return $order;
    }

    private function setTransaction(CSVObject $item, \LLMS_Order $order)
    {
        if ($this->getPlanSku($item) == self::LIFETIME_PLAN_SKU) {
            $this->setLifeTimeTranasaction($item, $order);
        } else {
            $this->setDefaultTransaction($item, $order);
        }
    }

    private function setDefaultTransaction(CSVObject $item, \LLMS_Order $order)
    {
        $transaction = $order->record_transaction([
            'amount' => $item->get('Amount') / 100,
            'customer_id' => $item->get('Customer ID'),
            'source_id' => $item->get('id'),
        ]);
        $this->setDate($transaction->get('id'), $item->get('Created (UTC)'));
        return $transaction;
    }

    private function setLifeTimeTranasaction(CSVObject $item, \LLMS_Order $order)
    {
        $amount = $item->get('Current Value') ? $item->get('Current Value') : 0;
        $transaction = $order->record_transaction([
            'amount' => $amount,
        ]);
        return $transaction;
    }

    private function setDate($id, $date)
    {        return wp_update_post([
            'ID' => $id,
            'post_date' => $date,
            'post_date_gmt' => get_gmt_from_date($date)
        ]);
    }

    public function getErrors()
    {
        return $this->_errors;
    }

    private function importSetUp()
    {
        set_time_limit(self::EXECUTION_LIMIT);
        ini_set('memory_limit', self::MEMORY_LIMIT);
    }

    public function save()
    {
        $this->importSetUp();
        foreach ($this->_items as $item) {
            if (!$plan = $this->getAccessPlan($item)) {
                $this->setError('plan', $item);
                continue;
            }
            if (!$user = $this->getUser($item)) {
                $this->setError('user', $item);
                continue;
            }
            $getaway = $this->getGetaway();
            $order = $this->setOrder($item);
            $order->init($user, $plan, $getaway);
            $this->setTransaction($item, $order);
            $this->studentEnrollment($order);
            $logInfo = $this->getLogInfo($item->id, $order);
            $this->_logger->logInfo($logInfo);
        }

    }

    protected function getLogInfo($item, $order)
    {
        if (is_object($order) && $order->get( 'user_id' ) && $order->get('product_id')) {
            $orderProduct = $order->get('product_id');
            $orderUserId = $order->get('user_id');
            return "Item : {$item} Product: {$orderProduct} User: {$orderUserId}";
        }
        return "Fail on item {$item}";
    }

    /**
     * Method to enroll student
     *
     * @param $order
     * @return void
     */
    protected function studentEnrollment($order)
    {
        if (is_object($order) && $order->get('user_id') && $order->get('product_id')) {
            llms_enroll_student($order->get('user_id'), $order->get('product_id'), 'order_' . $order->get('id'));
        }
    }
}