<?php

namespace BgStripeImport\Includes;

class CSVObject extends \ArrayObject
{

    public function __construct($input = [])
    {
        parent::__construct((array) $input, \ArrayObject::ARRAY_AS_PROPS);
    }

    public function get($name){
        if(!empty($this->{$name})){
            return $this->{$name};
        }
        return null;
    }

    public function set($name, $value){
        return $this->{$name} = $value;
    }

}