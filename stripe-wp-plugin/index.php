<?php
/**
 * Plugin Name: BG Stripe Import
 * Description: Plugin imports the stripe database from the csv file
 * Version: 0.1
 * Author: Blockgeeks
 * Author URI: https://blockgeeks.com/
 *
 */

require_once  __DIR__ . '/vendor/autoload.php';

if ( is_admin() ) {
    add_action('admin_menu', function(){
        add_menu_page(
            'Stripe Import',
            'Stripe Import',
            'manage_options',
            'brander_qa',
            function() {
                return require_once dirname(__FILE__) . '/templates/display.php';
            }
        );
    });

}


