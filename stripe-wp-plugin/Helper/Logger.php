<?php

namespace BgStripeImport\Helper;

use Monolog\Logger as MonlogLogger;
use Monolog\Handler\StreamHandler;

class Logger
{
    private $logFile;

    protected  $logger;

    public function __construct()
    {
        $this->logFile = dirname(__DIR__, 1) . '/logs/importProcess.log';
        $this->setupLogFile();
        $this->initLogger();

    }

    public function logInfo(string $text) :bool
    {
        if ($this->isLogAvailable()) {
            $this->logger->info($text);
            return true;
        }
        return false;
    }

    private function initLogger()
    {
        $this->logger = new MonlogLogger('importProcess');
        $this->logger->pushHandler(new StreamHandler($this->logFile, MonlogLogger::INFO));
    }

    private function isLogAvailable()
    {
        if (!file_exists($this->logFile)) {
            return false;
        }
       return true;
    }

    private function setupLogFile()
    {
        if (!file_exists( $this->logFile)) {
            touch($this->logFile);
            chmod( $this->logFile, 0777);
        }
    }
}