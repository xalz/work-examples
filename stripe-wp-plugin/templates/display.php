<?php

use BgStripeImport\Includes\CSV;
use BgStripeImport\Includes\StripeImport;
if ( ! is_admin() ) { exit; }

if (isset($_FILES) && count($_FILES)) {
    $csv = (new CSV(current($_FILES)))->parse();
    $import = new StripeImport($csv);
    $import->save();
}
?>

<style>
    .llms-admin-notice {
        display: none;
    }
</style>

<form method="POST" enctype="multipart/form-data">
    <input name="file" type="file">
    <button>Submit</button>
</form>

<?php if (isset($import) && $errors = $import->getErrors()): ?>
    <h3>Not Found Items</h3>
    <table>
        <tr>
            <th>Error</th>
            <th>Id</th>
            <th>Customer Email</th>
            <th>Customer ID</th>
        </tr>
        <?php foreach($errors as $error): ?>
        <tr>
            <td>
                <?php switch ($error->get('type')){
                    case 'plan': echo 'Access Plan not found'; break;
                    case 'user': echo 'User not found'; break;
                } ?>
            </td>
            <td><?php echo $error->get('id'); ?></td>
            <td><?php echo $error->get('Customer Email'); ?></td>
            <td><?php echo $error->get('Customer ID'); ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>


