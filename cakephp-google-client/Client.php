<?php

namespace App\Utility\Client\Google;

use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\InstanceConfigTrait;

/**
 * Class Client
 * @package App\Utility\Google
 */
abstract class Client
{
    use InstanceConfigTrait;

    protected const NAME = '';
    protected const TOKEN = 'token';

    protected $_defaultConfig = [
        'config' => 'console',
        'access' => 'offline',
        'scopes' => [],
    ];

    private $_client;
    private $_token;

    /**
     * Client constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this
            ->setConfig($config)
            ->setClient()->setScopes()->setApplicationName()->setAccessType()
            ->setClientId()->setClientSecret()->setRedirectUri()->setState();
    }

    /**
     * @param callable $callback
     * @return $this
     */
    public function auth(callable $callback)
    {
        if($token = $this->getToken()){
            $this->getClient()->setAccessToken($token);
        }
        if($this->getClient()->isAccessTokenExpired()){
            if ($refresh = $this->getClient()->getRefreshToken()) {
                $this->setTokenByRefresh($refresh);
            } else {
                $this->setTokenByCallback($callback);
            }
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getToken(): array
    {
        switch ($this->getConfig('config'))
        {
            case 'browser':
                return $this->_token ?? [];

            case 'console':
            default:
                return Cache::read(':google:' . $this::NAME, $this::TOKEN) ?: [];
        }
    }

    /**
     * @param array $token
     * @return array
     */
    public function setToken(array $token): array
    {
        switch ($this->getConfig('config'))
        {
            case 'browser':
                $this->_token = $token;
                break;

            case 'console':
            default:
                Cache::write(':google:' . $this::NAME, $token, $this::TOKEN);
                break;
        }
        return $token;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->getClient()->createAuthUrl();
    }

    /**
     * @param string $refresh
     * @return Client
     */
    protected function setTokenByRefresh(string $refresh): Client
    {
        $this->getClient()->fetchAccessTokenWithRefreshToken($refresh);
        $this->setToken($this->getClient()->getAccessToken());
        return $this;
    }

    /**
     * @param callable $callback
     * @return Client
     */
    protected function setTokenByCallback(callable $callback): Client
    {
        if($code = $callback($this->getClient()->createAuthUrl())){
            $this->getClient()->setAccessToken(
                $this->getClient()->fetchAccessTokenWithAuthCode($code)
            );
            $this->setToken($this->getClient()->getAccessToken());
        }
        return $this;
    }

    /**
     * @return Client
     */
    protected function setApplicationName(): Client
    {
        $this->getClient()->setApplicationName($this::NAME);
        return $this;
    }

    /**
     * @return Client
     */
    protected function setState(): Client
    {
        $this->getClient()->setState($this->getConfigure('state'));
        return $this;
    }

    /**
     * @return Client
     */
    protected function setAccessType(): Client
    {
        $this->getClient()->setAccessType($this->getConfigure('access'));
        return $this;
    }

    /**
     * @return Client
     */
    protected function setScopes(): Client
    {
        $this->getClient()->setScopes($this->getConfigure('scopes'));
        return $this;
    }

    /**
     * @return Client
     */
    protected function setClientId(): Client
    {
        $this->getClient()->setClientId($this->getConfigure('id'));
        return $this;
    }

    /**
     * @return Client
     */
    protected function setClientSecret(): Client
    {
        $this->getClient()->setClientSecret($this->getConfigure('secret'));
        return $this;
    }

    /**
     * @return Client
     */
    protected function setRedirectUri(): Client
    {
        $this->getClient()->setRedirectUri($this->getConfigure('redirect'));
        return $this;
    }

    /**
     * @return Client
     */
    protected function setClient(): Client
    {
        $this->_client = new \Google_Client();
        return $this;
    }

    /**
     * @return \Google_Client
     */
    protected function getClient(): \Google_Client
    {
        return $this->_client;
    }

    /**
     * @param string $name
     * @return mixed
     */
    protected function getConfigure(string $name)
    {
        return $this->getConfig($name,
            Configure::read(sprintf('Google.%s.%s', $this->getConfig('config'), $name))
        );
    }
}
