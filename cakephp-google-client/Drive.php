<?php

namespace App\Utility\Client\Google;

use Cake\Collection\Collection;
use Cake\Filesystem\File;
use GuzzleHttp\Psr7\Response;

/**
 * Class Drive
 * @package App\Utility\Google
 */
class Drive extends Client
{
    protected const NAME = 'drive';

    protected $_defaultConfig = [
        'config' => 'console',
        'access' => 'offline',
        'scopes' => [
            \Google_Service_Drive::DRIVE
        ],
    ];

    private $_service;

    /**
     * Drive constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->setService();
    }

    /**
     * @param string $path
     * @return \Google_Service_Drive_DriveFile
     */
    public function set(string $path): \Google_Service_Drive_DriveFile
    {
        $new = $this->getNewFile();
        $file = $this->getFile($path);
        $new->setName($this->getFileName($file));
        return $this->getService()->files->create($new, [
            'data' => $file->read(),
            'mimeType' => $file->mime()
        ]);
    }

    /**
     * @param array $options
     * @return Collection
     */
    public function list(array $options): Collection
    {
        return $this->getCollection(
            (array) $this->getService()->files->listFiles($options)->getFiles()
        );
    }

    /**
     * @param string $id
     * @return Response
     */
    public function get(string $id): Response
    {
        /**
         * @var Response $file
         */
        $file = $this->getService()->files->get($id, [
            'alt' => 'media'
        ]);
        return $file;
    }

    /**
     * @return \Google_Service_Drive_DriveFile
     */
    protected function getNewFile(): \Google_Service_Drive_DriveFile
    {
        return new \Google_Service_Drive_DriveFile();
    }

    /**
     * @param array $values
     * @return Collection
     */
    protected function getCollection(array $values): Collection
    {
        return new Collection($values);
    }

    /**
     * @param string $name
     * @return File
     */
    protected function getFile(string $name): File
    {
        return new File($name);
    }

    /**
     * @param File $file
     * @return string
     */
    protected function getFileName(File $file): string
    {
        return sprintf('%s.%s', $file->name(), $file->ext());
    }

    /**
     * @return \Google_Service_Drive
     */
    protected function getService(): \Google_Service_Drive
    {
        return $this->_service;
    }

    /**
     * @return Drive
     */
    protected function setService(): Drive
    {
        $this->_service = new \Google_Service_Drive($this->getClient());
        return $this;
    }

}
