<?php

namespace App\Utility\Client\Google;

/**
 * Class Plus
 * @package App\Utility\Google
 */
class Plus extends Client
{
    protected const NAME = 'plus';

    protected $_defaultConfig = [
        'config' => 'browser',
        'access' => 'online',
        'scopes' => [
            \Google_Service_Plus::USERINFO_EMAIL,
            \Google_Service_Plus::USERINFO_PROFILE
        ],
    ];

    private $_service;

    /**
     * Plus constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->setService();
    }

    /**
     * @param string $id
     * @return \Google_Service_Plus_Person
     */
    public function get(string $id): \Google_Service_Plus_Person
    {
        return $this->getService()->people->get($id);
    }

    /**
     * @return \Google_Service_Plus
     */
    protected function getService(): \Google_Service_Plus
    {
        return $this->_service;
    }

    /**
     * @return Plus
     */
    protected function setService(): Plus
    {
        $this->_service = new \Google_Service_Plus($this->getClient());
        return $this;
    }

}
