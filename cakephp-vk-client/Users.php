<?php

namespace App\Utility\Client\Vk;

use Cake\Core\InstanceConfigTrait;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

/**
 * Class Users
 * @package App\Utility\Vk
 */
class Users extends Client
{
    use InstanceConfigTrait;

    protected $_defaultConfig = [
        'version' => '5.37',
        'config' => 'browser',
        'display' => VKOAuthDisplay::PAGE,
        'response' => VKOAuthResponseType::CODE,
        'scopes' => [
            VKOAuthUserScope::EMAIL,
        ],
    ];

    /**
     * @param array $params
     * @return \ArrayIterator|\ArrayObject
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function get(array $params)
    {
        return $this->response(
            $this->getClient()->users()->get($this->getToken()->offsetGet('access_token'), $params)
        );
    }

    /**
     * @param array $response
     * @return \ArrayIterator|\ArrayObject
     */
    protected function response(array $response)
    {
        if (!$this->isAssoc($response)) {
            foreach ($response as &$value) {
                $value = new \ArrayObject($value);
            }
            return new \ArrayIterator($response);
        }
        return new \ArrayObject($response);
    }

    /**
     * @param array $array
     * @return bool
     */
    protected function isAssoc(array $array): bool
    {
        $keys = array_keys($array);
        return $keys !== array_keys($keys);
    }

}
