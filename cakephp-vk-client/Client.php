<?php

namespace App\Utility\Client\Vk;

use Cake\Core\Configure;
use Cake\Core\InstanceConfigTrait;
use VK\Client\VKApiClient;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

/**
 * Class Client
 * @package App\Utility\Vk
 */
abstract class Client
{

    protected const LANG = 'ru';

    use InstanceConfigTrait;

    protected $_defaultConfig = [
        'version' => '5.80',
        'config' => 'console',
        'display' => VKOAuthDisplay::PAGE,
        'response' => VKOAuthResponseType::CODE,
        'scopes' => [],
    ];

    private $_client;
    private $_token;

    /**
     * Client constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->setConfig($config)->setClient();
    }

    /**
     * @param callable $callback
     * @return $this
     * @throws \VK\Exceptions\VKClientException
     * @throws \VK\Exceptions\VKOAuthException
     */
    public function auth(callable $callback)
    {
        if (!$this->getToken()) {
            $this->setTokenByCallback($callback);
        }
        return $this;
    }

    /**
     * @return \ArrayObject|null
     */
    public function getToken(): ?\ArrayObject
    {
        return $this->_token;
    }

    /**
     * @param array $token
     * @return $this
     */
    public function setToken(array $token)
    {
        $this->_token = new \ArrayObject($token);
        return $this;
    }

    /**
     * @param string|null $token
     * @return $this
     */
    public function setAccessToken(string $token = null)
    {
        return $this->setToken([
            'access_token' => $token ?: $this->getConfigure('token')
        ]);
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->getToken()->offsetGet('access_token');
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->getOAuth()->getAuthorizeUrl(
            $this->getConfigure('response'),
            $this->getConfigure('id'),
            $this->getConfigure('redirect'),
            $this->getConfigure('display'),
            $this->getConfigure('scopes'),
            $this->getConfigure('state'),
            $this->getConfigure('groups')
        );
    }

    /**
     * @param array $response
     * @return \ArrayObject|\ArrayIterator
     */
    protected function response(array $response)
    {
        $response = $this->each($response);
        if (!$this->assoc($response)) {
            return new \ArrayIterator($response);
        }
        return new \ArrayObject($response);
    }

    /**
     * @param array $response
     * @return array
     */
    protected function each(array $response)
    {
        foreach ($response as &$value) {
            if (is_array($value)) {
                $value = $this->response($value);
            }
            unset($value);
        }
        return $response;
    }

    /**
     * @param array $array
     * @return bool
     */
    protected function assoc(array $array): bool
    {
        $keys = array_keys($array);
        return $keys !== array_keys($keys);
    }

    /**
     * @param callable $callback
     * @return Client
     * @throws \VK\Exceptions\VKClientException
     * @throws \VK\Exceptions\VKOAuthException
     */
    protected function setTokenByCallback(callable $callback): Client
    {
        if ($code = $callback($this->getUrl())) {
            $this->setToken($this->getOAuth()->getAccessToken(
                $this->getConfigure('id'),
                $this->getConfigure('secret'),
                $this->getConfigure('redirect'),
                $code
            ));
        }
        return $this;
    }

    /**
     * @return VKOAuth
     */
    protected function getOAuth()
    {
        return new VKOAuth($this->getConfig('version'));
    }

    /**
     * @return Client
     */
    protected function setClient(): Client
    {
        $this->_client = new VKApiClient($this->getConfig('version'), $this::LANG);
        return $this;
    }

    /**
     * @return VKApiClient
     */
    protected function getClient(): VKApiClient
    {
        return $this->_client;
    }

    /**
     * @param string $name
     * @param null $default
     * @return mixed
     */
    protected function getConfigure(string $name, $default = null)
    {
        return $this->getConfig($name,
            Configure::read(sprintf('Vk.%s.%s', $this->getConfig('config'), $name), $default)
        );
    }
}
