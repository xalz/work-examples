<?php

namespace App\Utility\Client\Vk;

use Cake\Core\InstanceConfigTrait;
use Cake\Utility\Security;
use WSSC\{
    Components\ClientConfig, WebSocketClient, Exceptions
};

/**
 * Class Streaming
 * @package App\Utility\Vk
 */
class Streaming extends Client
{
    use InstanceConfigTrait;

    protected $_defaultConfig = [
        'version' => '5.80',
        'config' => 'console',
        'while' => true,
        'scopes' => [],
    ];

    private $_wss;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->setAccessToken()->setWss();
    }

    /**
     * @param callable $callback
     * @return Streaming
     * @throws Exceptions\BadOpcodeException
     * @throws Exceptions\BadUriException
     * @throws Exceptions\ConnectionException
     */
    public function run(callable $callback): Streaming
    {
        while ($this->getConfig('while')) {
            try {
                $payload = $this->getWss()->receive();
            } catch (Exceptions\ConnectionException $e) {
                continue;
            }
            if (!$response = $this->getPayload($payload)) {
                $pong = Security::randomBytes(1);
                $this->getWss()->send($pong, 'pong');
                continue;
            }
            $callback($response);
        }
        return $this;
    }

    /**
     * @param string $payload
     * @return null|\ArrayObject
     * @throws \Exception
     */
    protected function getPayload(string $payload)
    {
        if($response = json_decode($payload)){
            switch ($response->code ?? null) {
                case 100:
                    return $response->event;
                case 300:
                    $error = $response->service_message;
                    throw new \Exception("Error message: {$error}");
                    break;
            }
        }
        return null;
    }

    /**
     * @return \ArrayObject
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    protected function getServers(): \ArrayObject
    {
        return $this->response(
            $this->getClient()->streaming()->getServerUrl(
                $this->getToken()->offsetGet('access_token')
            )
        );
    }

    protected function getHost(\ArrayObject $response): string
    {
        return sprintf('wss://%s/stream?key=%s',
            $response->offsetGet('endpoint'),
            $response->offsetGet('key')
        );
    }

    /**
     * @return Streaming
     * @throws Exceptions\BadUriException
     * @throws Exceptions\ConnectionException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    protected function setWss(): Streaming
    {
        $this->_wss = new WebSocketClient($this->getHost($this->getServers()), new ClientConfig());
        return $this;
    }

    /**
     * @return WebSocketClient
     */
    protected function getWss(): WebSocketClient
    {
        return $this->_wss;
    }


}
