<?php

namespace FileUpload;

class File extends \ArrayObject {

    const fields = [
        'id', 'name', 'type', 'size', 'path', 'created'
    ];

    public function __construct($input = []){
        parent::__construct((array) $input, \ArrayObject::ARRAY_AS_PROPS);

    }

    public function get($name){
        if(!empty($this->{$name})){
            return $this->{$name};
        }
        return null;
    }

    public function set($name, $value){
        return $this->{$name} = $value;
    }

    public function toArray(){
        return array_filter((array) $this, function($key) {
            return in_array($key, $this::fields);
        }, ARRAY_FILTER_USE_KEY);
    }

}