<?php

namespace FileUpload;

class Base extends \PDO {

    public function __construct($host, $db, $user, $pass, $charset){
        $dsn = sprintf('mysql:host=%s;dbname=%s;charset=%s', $host, $db, $charset);

        parent::__construct($dsn, $user, $pass, [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ]);

    }

    public function findById($table, $id){
        $sth = $this->prepare(join(' ', [
            'SELECT id, name, path, type, size, path, created FROM',
            $table, 'WHERE id = :id'
        ]));
        $sth->execute([ 'id' => $id ]);
        return $sth->fetch();
    }

    public function save($table, array $values){
        $sth = $this->prepare(join(' ', [
            'INSERT INTO', $table, '(', join(',', array_keys($values)), ')',
            'VALUES (', join(',', array_map(function ($key){
                return ':' . $key;
            }, array_keys($values))), ')'
        ]));
        if($sth->execute($values)){
            return $this->lastInsertId();
        }
        return false;
    }



}