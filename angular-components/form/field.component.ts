import {Component, Input, OnInit} from '@angular/core';

import {RestService} from "../../services/rest.service";
import {DictionaryListResource} from "../../contns/resources.const";
import {RequestType} from "../../types/request.type";
import {ResponseType} from "../../types/response.type";
import {DictionaryType} from "../../types/dictionary.type";
import {FormControl} from "@angular/forms";

@Component({
  'template': ''
})
export class FormFieldComponent implements OnInit {

  @Input() dictionary: string;
  @Input() required: boolean;
  @Input() label: string;
  @Input() control: FormControl;

  public items: DictionaryType[];

  /**
   * @param RestService
   */
  constructor(private RestService: RestService) {
  }

  public ngOnInit() {
    if (this.dictionary) this.getDictionaryList();
  }

  /**
   * Request to rest
   */
  protected getDictionaryList = () => this.RestService
    .request(DictionaryListResource, this.getDictionaryRequest()).subscribe(this.getDictionaryHandler);

  /**
   * Body parameters
   */
  protected getDictionaryRequest = (): RequestType => ({
    query: {
      code: this.dictionary
    }
  });

  /**
   * Response handler
   * @param response
   */
  protected getDictionaryHandler = ({data: {list: items}}: ResponseType) => {
    this.items = items as DictionaryType[];
  };

}
