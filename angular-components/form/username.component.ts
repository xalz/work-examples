import {Component} from '@angular/core';
import {FormFieldComponent} from "./field.component";

@Component({
  selector: 'form-username',
  templateUrl: './username.component.html'
})
export class FormUsernameComponent extends FormFieldComponent {

  public mask: string;
  protected phone = '+0(000)000-0000';

  /**
   * Set/remove mask
   * @param event
   */
  public onKeyDown(event) {
    if (parseInt(this.control.value)) {
      return this.mask = this.phone;
    }
    delete this.mask;
  }

}
