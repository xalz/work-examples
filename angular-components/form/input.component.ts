import {Component, Input} from '@angular/core';

import {FormFieldComponent} from "./field.component";

@Component({
  selector: 'form-input',
  templateUrl: './input.component.html'
})
export class FormInputComponent extends FormFieldComponent {
  @Input() type: string;
  @Input() mask: string;
}
