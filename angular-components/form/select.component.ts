import {Component} from '@angular/core';

import {FormFieldComponent} from "./field.component";

@Component({
  selector: 'form-select',
  templateUrl: './select.component.html'
})
export class FormSelectComponent extends FormFieldComponent {

}
