import {Component, Input, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

import {RestService} from "../../services/rest.service";
import {RestorePasswordResource} from "../../contns/resources.const";
import {ResponseType} from "../../types/response.type";
import {RequestType} from "../../types/request.type";

@Component({
  selector: 'restore-form',
  templateUrl: './restore.component.html'
})
export class RestoreComponent implements OnInit {

  @Input() onRestore;

  public form: FormGroup;

  protected fields: object = {
    username: ['', Validators.required]
  };

  /**
   * @param FormBuilder
   * @param RestService
   */
  constructor(
    private FormBuilder: FormBuilder,
    private RestService: RestService,
  ) {
  }

  public ngOnInit() {
    this.initForm();
  };

  /**
   * Submit event
   */
  public onSubmit = () => {
    if (this.form.invalid) {
      return;
    }
    this.onRequest();
  };

  /**
   * Init form
   */
  protected initForm = () => {
    this.form = this.FormBuilder.group(this.fields);
  };

  /**
   * Request to rest
   */
  protected onRequest = () => this.RestService
    .request(RestorePasswordResource, this.getRequest()).subscribe(this.getHandler);

  /**
   * Body parameters
   */
  protected getRequest = (): RequestType => ({
    body: this.form.value
  });

  /**
   * Response handler
   * @param response
   */
  protected getHandler = (response: ResponseType) => {
    console.log(response);
  };

}
