import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AuthSocialResource} from "../../contns/resources.const";
import {ResponseType} from "../../types/response.type";
import {RestService} from "../../services/rest.service";
import {RequestType} from "../../types/request.type";
import {UserType} from "../../types/user.type";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'auth-social',
  templateUrl: './social.component.html'
})
export class SocialComponent implements OnInit {

  state: string;
  code: string;

  /**
   * @param ActivatedRoute
   * @param RestService
   * @param UserService
   */
  constructor(
    private ActivatedRoute: ActivatedRoute,
    private RestService: RestService,
    private UserService: UserService
  ) {
    this.ActivatedRoute.queryParams.subscribe(({code, state}) => {
      this.state = state;
      this.code = code;
    });
  }

  public ngOnInit() {
    if (this.code) this.onRequest(this.state);
  }

  /**
   * Request to rest
   * @param state string
   */
  public onRequest = (state: string) => this.RestService
    .request(AuthSocialResource, this.getRequest(state))
    .subscribe(this.getHandler);

  /**
   * Click event on button
   * @param state
   */
  public onClick = (state: string) => {
    delete this.code;
    this.onRequest(state);
  };

  /**
   * Body parameters
   * @param state string
   */
  protected getRequest = (state: string): RequestType => ({
    body: {
      state: state,
      code: this.code
    }
  });

  /**
   * handle rest response
   * @param response ResponseType
   */
  protected getHandler = (response: ResponseType): void => {
    if (response.data.url) return window.location.href = response.data.url;
    this.UserService.set(response.data as UserType);
  }

}
