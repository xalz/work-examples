import {Component, Input, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';

import {RestService} from "../../services/rest.service";
import {AuthResource} from "../../contns/resources.const";
import {ResponseType} from "../../types/response.type";
import {UserType} from "../../types/user.type";
import {UserService} from "../../services/user.service";
import {RequestType} from "../../types/request.type";

@Component({
  selector: 'auth-form',
  templateUrl: './auth.component.html'
})
export class AuthComponent implements OnInit {

  @Input() onRestore;

  public form: FormGroup;

  protected fields: any = {
    username: ['', Validators.required],
    password: ['', Validators.required]
  };

  /**
   * @param FormBuilder
   * @param RestService
   * @param UserService
   */
  constructor(
    private FormBuilder: FormBuilder,
    private RestService: RestService,
    private UserService: UserService
  ) {}

  public ngOnInit() {
    this.initForm();
  };

  /**
   * Submit event
   */
  public onSubmit = () => {
    if (this.form.invalid) {
      return;
    }
    this.onRequest();
  };

  /**
   * Init form
   */
  protected initForm = () => {
    this.form = this.FormBuilder.group(this.fields);
  };

  /**
   * Request to rest
   */
  protected onRequest = () => this.RestService
    .request(AuthResource, this.getRequest()).subscribe(this.setUser);

  /**
   * Body parameters
   */
  protected getRequest = (): RequestType => ({
    body: this.form.value
  });

  /**
   * set user to UserService
   * @param user
   */
  protected setUser = ({data: user}: ResponseType) => this.UserService.set(user as UserType);

}
