import {Component, Input, OnInit} from '@angular/core';
import {forkJoin, from} from 'rxjs';

import {DictionaryViewResource, CityViewResource} from "../../contns/resources.const";
import {RestService} from "../../services/rest.service";
import {AttributeType, OfferType} from "../../types/offer.type";
import {RequestType} from "../../types/request.type";

import * as _ from 'lodash';

@Component({
  selector: 'offer-title',
  template: '{{title}}',
})
export class OfferTitleComponent implements OnInit {

  @Input() offer: OfferType;

  public title: string;

  constructor(private RestService: RestService) {
  }

  ngOnInit() {
    this.setTitle();
  }

  /**
   * Set combine title requests
   */
  protected setTitle = () => forkJoin(this.getDictionary(), this.getCity()).subscribe(this.getHandler);

  /**
   * Get city id in offer
   */
  protected getCityId = (): number => this.offer.location.city.id;

  /**
   * Get type id in type attribute
   */
  protected getTypeId = (): number => this.getType().dictionary.id;

  /**
   * Find type in offer attributes
   */
  protected getType = (): AttributeType => {
    return _.find(this.offer.attributes, attribute => attribute.category.code === 'offer_type');
  };

  /**
   * Get Dictionary from rest
   */
  protected getDictionary = () => this.RestService.request(DictionaryViewResource, this.getRequest(this.getTypeId()));

  /**
   * Get City from rest
   */
  protected getCity = () => this.RestService.request(CityViewResource, this.getRequest(this.getCityId()));

  /**
   * Request parameters
   * @param id number
   */
  protected getRequest = (id: number): RequestType => ({
    params: {id}
  });

  protected getHandler = (response) => {
    console.log(response);
  };

}
