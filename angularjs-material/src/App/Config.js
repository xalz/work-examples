var config = {

  api:{
    setBaseUrl:'http://api.4arenda.ru/',
    setRequestSuffix:'.json',
    setDefaultHttpFields:{
      cache: true
    }
  },

  map:{
    key:'AIzaSyBYwcxh-a9_UH3HngMVxYRpY5DUTVkPzQ8',
    language:'ru',
    libraries:'geometry,drawing,places,visualization'
  },

  themes:{
    new4arenda:{
      '50': '#5dcef8',
      '100': '#13b8f5',
      '200': '#0894c8',
      '300': '#056184',
      '400': '#044c66',
      '500': '#3B3B3B',
      '600': '#02202c',
      '700': '#010b0e',
      '800': '#000000',
      '900': '#000000',
      'A100': '#5dcef8',
      'A200': '#13b8f5',
      'A400': '#044c66',
      'A700': '#010b0e',
      'contrastDefaultColor': 'light',
      'contrastDarkColors': '50 100 A100 A200'
    },
    default:{
      primaryPalette:{
        name:'new4arenda'
      },
      accentPalette:{
        name:'new4arenda'
      }
    }
  },

  html5Mode:{
    enabled:true,
    requireBase:false
  },

  states:{
    'Main':{
      url:'',
      abstract:true,
      templateUrl:'/View/Layout.html'
    },
    'Main.Index':{
      url:'/',
      views:{
        Content:{
          templateUrl:'/View/Pages/Index.html',
          controller:'IndexCtrl'
        }
      }
    },
    'Main.OfferIndex':{
      url:'/offers?{page:int}&{rooms:int}&{region:int}&{city:int}&{tags:int}&{type:int}&{period:int}&{price:int}&{floor:int}&{area:int}&geometry',
      params:{
        page:{ value:1, dynamic:true },
        tags:{ value:[], array:true },
        rooms:{ value:[], array:true }
      },
      views:{
        Content:{
          templateUrl:'/View/Pages/Offer/Index.html',
          controller:'OfferIndexCtrl'
        }
      }
    },
    'Main.OfferAdd':{
      url:'/offers/edit',
      views:{
        Content:{
          templateUrl:'/View/Pages/Offer/Edit.html',
          controller:'OfferEditCtrl'
        }
      }
    },
    'Main.OfferEdit':{
      url:'/offers/edit/{id:int}',
      views:{
        Content:{
          templateUrl:'/View/Pages/Offer/Edit.html',
          controller:'OfferEditCtrl'
        }
      }
    },
    'Main.OfferView':{
      url:'/offers/{id:int}',
      views:{
        Content:{
          templateUrl:'/View/Pages/Offer/View.html',
          controller:'OfferViewCtrl'
        }
      }
    },
    'Main.DashboardIndex':{
      url:'/dashboard',
      views:{
        Content:{
          templateUrl:'/View/Pages/Dashboard/Index.html',
          controller:'DashboardIndexCtrl'
        }
      }
    },
    'Main.DashboardAuth':{
      url:'/dashboard/auth',
      views:{
        Content:{
          templateUrl:'/View/Pages/Dashboard/Auth.html',
          controller:'DashboardAuthCtrl'
        }
      }
    },
  }
};