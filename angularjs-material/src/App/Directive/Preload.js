app.directive('preload', function(){
  return {
    restrict: 'EA',
    scope:{
      model:'=',
      align:'@',
      diameter:'@'
    },
    template:[
      '<div layout="row" class="material-preload" layout-align="{{align}}" ng-if="show">',
        '<md-progress-circular md-diameter="{{diameter}}" md-mode="indeterminate" class=""></md-progress-circular>',
      '</div>'
    ].join(''),
    controller:function($scope) {
      $scope.show = true;
      $scope.$watch('model', function(){
        $scope.show = _.isUndefined($scope.model);
      });
    }
  };
});