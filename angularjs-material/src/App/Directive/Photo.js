app.directive('photo', function(){
  return {
    restrict: 'EA',
    scope:{
      src:'=',
      width:'=',
      height:'=',
      alt:'&'
    },
    template:'<img ng-src="{{host}}{{photo.url}}" ng-if="showImg()" />',
    controller:[
      '$scope', '$Photo',
      function($scope, $Photo){

        $scope.model = new $Photo();
        $scope.host = config.api ? config.api.setBaseUrl : null;

        $scope.findMain = function(){
          return _.find($scope.src, {
            is_main:true
          }) || _.head($scope.src);
        };

        $scope.findSize = function() {
          return $scope.main ? _.find($scope.main.sizes, {
            width:$scope.width,
            height:$scope.height
          }) : null;
        };

        $scope.showImg = function(){
          return $scope.photo && $scope.photo.url;
        };

        $scope.$watchCollection('[src, width, height]', function(){
          if(!_.isUndefined($scope.src)){
            if(!_.isArray($scope.src)){
              $scope.main = $scope.src;
            } else {
              $scope.main = $scope.findMain();
            }
            $scope.photo = $scope.findSize();
            if($scope.main && !$scope.photo){
              $scope.photo = new $Photo();
              $scope.photo.resize($scope.main.id, $scope.width, $scope.height);
            }
          }
        });
      }
    ]
  };
});