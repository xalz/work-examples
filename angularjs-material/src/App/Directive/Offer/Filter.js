app.directive('offerFilter', function(){
  return {
    restrict: 'EA',
    scope:{
      _filter:'=filter',
    },
    templateUrl:'/View/Widgets/Offer/Filter.html',
    controller:[
      '$scope', '$Offer', '$Dictionary', '$Map', '$state',
      function($scope, $Offer, $Dictionary, $Map, $state){

        $scope.map = $Map;
        $scope.counts = new $Offer();
        $scope.filter = _.clone($scope._filter || {
          tags:[],
          rooms:[]
        });

        $scope.submit = function(){
          $scope.filter.page = 1;
          $state.go('Main.OfferIndex', $scope.filter);
        };

        $scope.room = {
          checked:function(id){
            if(!_.isArray($scope.filter.rooms)){
              $scope.filter.rooms = [$scope.filter.rooms];
            }
            if(id) return _.indexOf($scope.filter.rooms, id) >= 0;
            return $scope.filter.rooms.length === 0;
          },
          toggle:function(id, length){
            if(!id || length <= $scope.filter.rooms.length + 1) {
              $scope.filter.rooms = [];
            } else {
              if(!this.checked(id)){
                $scope.filter.rooms.push(id);
              } else {
                _.remove($scope.filter.rooms, function(r_id){
                  return id === r_id;
                });
              }
            }
          }
        };

        $scope.geometry = {
          set:function(geometry){
            if(!geometry){
              $scope.filter.geometry = null;
            } else {
              $scope.filter.geometry = $scope.map.control.geometry.encode(
                geometry.getPath()
              );
            }
          },
          get:function(){
            return $scope.filter.geometry;
          }
        };

        $scope.slider = {
          disabled:function(num){
            return !_.isNumber(num) || num <= 0;
          }
        };

        $scope.details = {
          show:false,
          trigger:function(){
            this.show = !this.show;
          },
          isShow:function(){
            return this.show;
          }
        };

        _.forEach({
          types:{
            code:'offer_type'
          },
          periods:{
            code:'offer_period'
          },
          rooms:function(item){
            return item.code === 'offer_rooms' && item.alias(3);
          }
        }, function(filter, key){
          $scope[key] = new $Dictionary();
          $scope[key].list(filter, function(item){
            return item.alias(3);
          });
        });

        $scope.$watchCollection(function(){
          return [
            $scope.filter.region,
            $scope.filter.city,
            $scope.filter.period,
            $scope.filter.type,
            $scope.filter.geometry,
            $scope.filter.rooms.length,
            $scope.filter.tags.length
          ];
        }, function(){
          $scope.counts.counts($scope.filter);
          new $Offer().points($scope.filter).then(function(points){
            $scope.markers = [];
            _.forEach(points, function(point){
              $scope.markers.push({
                id:point.id,
                latitude:point.geometry.coordinates[1],
                longitude:point.geometry.coordinates[0]
              });
            });
          });

        });

      }
    ]
  };
});