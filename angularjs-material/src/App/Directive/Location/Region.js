app.directive('locationRegion', function(){
  return {
    restrict: 'EA',
    scope:{
      geometry:'=',
      region:'=',
      city:'=',
      empty:'@'
    },
    templateUrl:'/View/Widgets/Location/Region.html',
    controller:[
      '$scope', '$Location', '$Map',
      function($scope, $Location, $Map){

        var location = new $Location();

        if(!$scope.region  && !$scope.city){
          location.current().then(function(city){
            $scope.region = city.region.id;
            $scope.city = city.id;
          });
        }

        $scope.isEmpty = function(){
          return String($scope.empty).length > 0;
        };

        $scope.regions = {
          get:function(){
            delete $scope.regions.data;
            return location.regions().then(function(regions){
              $scope.regions.data = regions;
              $scope.regions.geometry();
            });
          },
          geometry:function(region){
            if(!$scope.city || !$scope.geometry){
              region = region || _.find(this.data, {
                  id:$scope.region
                });
              if(region && region.boundary){
                $scope.geometry = $Map.control.geometry.encode(
                  $Map.control.geoJSON.encode(
                    region.boundary.geometry
                  )
                );
              } else {
                delete $scope.geometry;
              }
            }
          },
          set:function(){
            delete $scope.city;
            this.geometry();
          }
        };

        $scope.cities = {
          get:function(){
            delete $scope.cities.data;
            return location.cities($scope.region).then(function(cities){
              $scope.cities.data = cities;
              $scope.cities.geometry();
            });
          },
          geometry:function(city){
            city = city || _.find(this.data, {
                id:$scope.city
              });
            if(city && city.boundary){
              $scope.geometry = $Map.control.geometry.encode(
                $Map.control.geoJSON.encode(
                  city.boundary.geometry
                )
              );
            } else {
              delete $scope.geometry;
              $scope.regions.geometry();
            }
          },
          set:function(){
            this.geometry();
          }
        };

        $scope.$watchCollection(function(){
          return [
            $scope.region
          ];
        }, function(){
          if($scope.region){
            $scope.regions.get();
            $scope.cities.get();
          }
        });
      }
    ]
  };
});