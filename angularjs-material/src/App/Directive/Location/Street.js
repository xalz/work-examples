app.directive('locationStreet', function(){
  return {
    restrict: 'EA',
    scope:{
      geometry:'=',
      regionId:'=',
      cityId:'=',
      street:'=',
      house:'='
    },
    templateUrl:'/View/Widgets/Location/Street.html',
    controller:[
      '$scope', '$Location', '$Map',
      function($scope, $Location, $Map){

        $scope.map = $Map;

        $scope.region = {};
        $scope.city = {};

        $scope.find = [
          $scope.street,
          $scope.house
        ].join(', ');

        $scope.search = function(){
          return $scope.map.control.address([
            $scope.region.name,
            $scope.city.name,
            $scope.find
          ].join(', '));
        };

        $scope.$watch(function(){
          return $scope.selected;
        }, function(){
          if($scope.selected){
            if($scope.selected.route){
              $scope.street = $scope.selected.route.short_name;
            }
            if($scope.selected.street_number){
              $scope.house = $scope.selected.street_number.short_name;
            }
          }
        });

        $scope.$watchCollection(function(){
          return [
            $scope.regionId,
            $scope.cityId
          ];
        }, function(){
          new $Location().cities($scope.regionId).then(function(cities){
            $scope.city = _.find(cities, { id:$scope.cityId });
          });
          new $Location().regions().then(function(regions){
            $scope.region = _.find(regions, { id:$scope.regionId });
          });
        });

      }
    ]
  };
});