app.directive('paginate', function(){
  return {
    restrict: 'EA',
    scope:{
      model:'=',
      filter:'=',
      length:'=',
      align:'@'
    },
    templateUrl:'/View/Widgets/Paginate.html',
    controller:function($scope) {

      $scope.paging = $scope.model.paging || {};

      $scope.goPage = function(page){
        $scope.filter.page = page;
        $scope.paging.current = page;
      };

      $scope.goFirst = function(){
        $scope.goPage(1);
      };

      $scope.goLast = function(){
        $scope.goPage($scope.paging.count);
      };

      $scope.goPrev = function(){
        $scope.goPage($scope.paging.current - 1);
      };

      $scope.goNext = function(){
        $scope.goPage($scope.paging.current + 1);
      };

      $scope.showFirst = function(){
        return $scope.paging.current <= $scope.length / 2 + 1
      };

      $scope.showLast = function(){
        return $scope.paging.current >= $scope.paging.count - $scope.length / 2
      };

      $scope.showNext = function(){
        return $scope.paging.next;
      };

      $scope.showPrev = function(){
        return $scope.paging.prev;
      };

      $scope.showPaginate = function(){
        return $scope.paging.count >= 1;
      };

      $scope.$watchCollection(function(){
        return [
          $scope.model.paging,
          $scope.length,
          $scope.filter.page,
          $scope.model.length
        ];
      }, function() {

        $scope.paging = $scope.model.paging || {};
        $scope.length = Number($scope.length) || 10;
        $scope.pages = [];
        var min = $scope.paging.current - Math.ceil($scope.length / 2),
          max = $scope.paging.current + Math.ceil($scope.length / 2);

        if(min <= 0 || !min){
          min = 0;
          max = $scope.length;
        }

        if(max > $scope.paging.count + 1 && min >= 0){
          min = min - max + $scope.paging.count + 1;
          max = $scope.paging.count + 1;
        }

        _.forEach(new Array($scope.paging.count), function(value, key){
          if($scope.pages.length < $scope.length && key >= min && key < max){
            $scope.pages.push(key + 1);
          }
        });
      });
    }
  };
});