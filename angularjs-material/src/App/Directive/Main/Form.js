app.directive('customForm', function(){
  return {
    restrict: 'EA',
    scope:{
      model:'=',
      fields:'='
    },
    templateUrl:'/View/Widgets/Main/Form.html',
    controller:[
      '$scope', '$Offer', '$Dictionary',
      function($scope, $Offer, $Dictionary){

        $scope.dictionary = {
          data:{},
          get:function(code){
            this.data[code] = new $Dictionary();
            this.data[code].list({ code: code }, function(item){
              return item.name;
            });
          }
        };

        $scope.select = {
          set:function(field){
            console.log(arguments)
          },
          get:function(field){
            //console.log(field, _.get($scope.model, field))
            return _.get($scope.model, field);
          }
        };

      }
    ]
  };
});