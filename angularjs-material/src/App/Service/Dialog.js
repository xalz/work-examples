app.service('$Dialog', [
  '$mdDialog',
  function($mdDialog){
    return function(options){
      return $mdDialog.show({
        openFrom:_.assignIn(options.open, {
          top:-50,
          width:30,
          height:80
        }),
        closeTo:_.assignIn(options.close, {
          left: 1500
        }),
        templateUrl:'View/Widgets/Dialog.html',
        clickOutsideToClose:true,
        controller:function($scope, $mdDialog) {
          $scope.hide = function() {
            $mdDialog.hide();
          };

          if(_.isString(options.include)){
            $scope.include = options.include;
          }

          if(_.isString(options.title)){
            $scope.title = options.title;
          }

          if(_.isString(options.body)){
            $scope.body = options.body;
          }

          if(_.isFunction(options.controller)){
            options.controller($scope, $mdDialog);
          }
        }
      });
    }
  }
]);