app.service('$Model', [
  '$rootScope',
  function($rootScope){

    var $model = {

      _alias:function(item){
        return function(num) {
          if(!_.isObject(item)){
            return null;
          }
          if(_.isArray(item.aliases) && item.aliases[num]){
            return item.aliases[num].name;
          }
          return item.name
        }
      },

      alias:function(item){
        if(!_.isArray(this._aliases)){
          return false;
        }
        _.forEach(this._aliases, function(field) {
          _.assignIn(item[field], {
            alias:$model._alias(item[field])
          });
        });
        return true;
      },

      toArray:function(){
        return _.filter(this, function(item, key){
          return !_.isNaN(parseInt(key));
        });
      }

    };

    return $model
  }
]);