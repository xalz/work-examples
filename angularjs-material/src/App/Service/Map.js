app.service('$Map', [
  '$rootScope', 'uiGmapGoogleMapApi', '$q',
  function($rootScope, uiGmapGoogleMapApi, $q){

    var $map = {
      zoom:8,
      center:{
        latitude:55,
        longitude:37
      },
      options:{
        scrollwheel:true,
        disableDefaultUI:true,
        marker:{
          icon:'/Assets/Images/pointmap.png'
        },
        polygon:{
          strokeColor:"#3B3B3B",
          strokeOpacity:0.7,
          strokeWeight:2,
          clickable:false,
          editable:true,
          draggable:true,
          fillOpacity:0,
          zIndex:1
        },
        cluster: {
          minimumClusterSize:3,
          zoomOnClick:true,
          styles:[
            {
              url:"/Assets/Images/boxmap30.png",
              width:30,
              height:30,
              textColor:'black',
              textSize:10,
              fontFamily:'Roboto'
            },
            {
              url:"/Assets/Images/boxmap40.png",
              width:40,
              height:40,
              textColor:'black',
              textSize:11,
              fontFamily:'Roboto'
            },
            {
              url:"/Assets/Images/boxmap50.png",
              width:50,
              height:50,
              textColor:'black',
              textSize:12,
              fontFamily:'Roboto'
            },
            {
              url:"/Assets/Images/boxmap70.png",
              width:70,
              height:70,
              textColor:'black',
              textSize:12,
              fontFamily:'Roboto'
            }
          ],
          averageCenter:true,
          clusterClass:'cluster-icon'
        }
      },
      control:{
        zoom:{
          get:function(){
            return $map.control.getGMap().getZoom();
          },
          set:function(zoom){
            $map.control.getGMap().setZoom(zoom);
          },
          in:function(){
            this.set(this.get() + 1);
          },
          out:function(){
            this.set(this.get() - 1);
          },
          array:function(array){
            var bounds = new google.maps.LatLngBounds();
            array.forEach(function (coordinates) {
              bounds.extend(coordinates);
            });
            //wtf
            console.log($map.control.getGMap().fitBounds(bounds));
            //this.set($map.control.getGMap().fitBounds(bounds));
          }
        },
        center:{
          get:function(){
            $map.control.getGMap().getCenter();
          },
          set:function(center){
            $map.control.getGMap().setCenter(center);
          },
          array:function(array){
            var bounds = new google.maps.LatLngBounds();
            array.forEach(function (coordinates) {
              bounds.extend(coordinates);
            });
            this.set(bounds.getCenter());
          }
        },
        drawing:{
          draft:false,
          trigger:function() {
            this.draft = !this.draft;
          },
          is:{
            draft:function(){
              return $map.control.drawing.draft;
            },
            empty:function(){
              return $map.control.drawing.object.data.length === 0;
            }
          },
          remove:function(){
            $map.control.listener.remove();
            this.object.remove();
            this.draft = false;
          },
          object:{
            data:[],
            set:function(object){
              return this.data.push(object);
            },
            remove:function(id){
              if(_.isInteger(id)){
                this.data[id - 1].setMap(null);
                delete this.data[id - 1];
              } else {
                _.forEach(this.data, function(data){
                  data.setMap(null);
                });
                this.data = [];
              }
            }
          }
        },
        ready:function(){
          var deferred = $q.defer();
          uiGmapGoogleMapApi.then(function(maps) {
            deferred.resolve(maps);
          });
          return deferred.promise;
        },
        refresh:function(){
          google.maps.event.trigger($map.control.getGMap(), 'resize');
        },
        listener:{
          data:[],
          set:function(object, event, geometry){
            return this.data.push(google.maps.event.addListener(object, event, geometry));
          },
          remove:function(id){
            if(_.isInteger(id)){
              this.data[id - 1].remove();
              delete this.data[id - 1];
            } else {
              _.forEach(this.data, function(data){
                data.remove();
              });
              this.data = [];
            }
          }
        },
        geoJSON:{
          encode:function(geometry){
            if(_.isArray(geometry.coordinates)){
              var array = [];
              _.forEach(geometry.coordinates, function(coordinates){
                _.forEach(coordinates, function(coordinate){
                  array.push(new google.maps.LatLng(coordinate[1], coordinate[0]));
                });
              });
              return array;
            }
            return null;
          }
        },
        geometry:{
          decode:function(string){
            return google.maps.geometry.encoding.decodePath(string);
          },
          encode:function(array){
            return google.maps.geometry.encoding.encodePath(array);
          }
        },
        address:function(string){
          var geocoder = new google.maps.Geocoder(),
            deferred = $q.defer();
          geocoder.geocode({
            address:string
            //location:search_coords ? search_coords : null
          }, function(geocodes, status){
            var items = [];
            if (status == google.maps.GeocoderStatus.OK) {
              string ? geocodes = _.filter(geocodes, function(o){
                return _.indexOf([
                    'route', 'street_address'
                  ], _.head(o.types)) > -1;
              }) : geocodes;
              _.forEach(geocodes, function(geocode) {
                var address = {};
                _.forEach([
                  'route', 'street_number',
                  'postal_code', 'locality'
                ], function(type) {
                  address[type] = _.find(geocode.address_components, function(o){
                    return _.head(o.types) === type
                  });
                });
                _.assignIn(address, geocode.geometry, {
                  formatted:[
                    address.route
                      ? address.route.short_name
                      : null,
                    address.street_number
                      ? address.street_number.short_name
                      : null
                  ].join(', ')
                });
                items.push(address);
              });
            }
            deferred.resolve(items)
          });
          return deferred.promise;
        }
        
      },
      layers:{
        polygon:{
          listeners:function(polygon, geometry){
            $map.control.listener.set(polygon.getPath(), 'set_at', function() {
              geometry.set(polygon);
            });
            $map.control.listener.set(polygon.getPath(), 'insert_at', function() {
              geometry.set(polygon);
            });
            $map.control.listener.set(polygon.getPath(), 'remove_at', function() {
              geometry.set(polygon);
            });
            $map.control.listener.set(polygon.getPath(), 'drag', function() {
              geometry.set(polygon);
            });
            $map.control.drawing.object.set(polygon);
            $map.control.center.array(polygon.getPath());
            $map.control.zoom.array(polygon.getPath());
            geometry.set(polygon);
          },
          watcher:function(layer, geometry){
            $rootScope.$watch(function(){
              return $map.control.drawing.is.draft();
            }, function(){
              if(!$map.control.drawing.is.draft()){
                layer.setDrawingMode(null);
              } else {
                layer.setDrawingMode('polygon');
                $map.control.listener.set(layer, 'polygoncomplete', function(polygon) {
                  $map.layers.polygon.listeners(polygon, geometry);
                  $map.control.drawing.trigger();
                });
              }
            });
            $rootScope.$watch(function(){
              return geometry.get();
            }, function(){
              $map.control.drawing.object.remove();
              if(geometry.get()){
                var polygon = new google.maps.Polygon(_.assignIn($map.options.polygon, {
                  editable:false
                }));
                polygon.setPath($map.control.geometry.decode(geometry.get()));
                $map.layers.polygon.listeners(polygon, geometry);
                polygon.setMap($map.control.getGMap());
              }
            });
            $rootScope.$watch(function(){
              return $map.control.drawing.is.empty();
            }, function(){
              if($map.control.drawing.is.empty()){
                geometry.set(null);
              }
            });
          },
          init:function(geometry){
            return function(layer){
              $map.layers.polygon.watcher(layer, geometry);
              $map.control.refresh();
              layer.setOptions({
                drawingControl:false,
                polygonOptions:$map.options.polygon
              });
            };
          }
        }
      }
    };

    return $map;
  }
]);