app.service('$API', [
  'Restangular', '$Storage', '$mdToast',
  function(Restangular, $Storage, $mdToast){

    var $api = Restangular.withConfig(function(setting){
      _.forEach(config.api, function(value, key) {
        setting[key](value);
      });
    });

    _.assignIn($api, {
      _params:function(params, name){
        var result = {};
        if(!_.isObject(params)){
          result[name] = params;
        } else {
          _.forEach(params, function(value, key){
            key = name ? name+'['+key+']' : key;
            if(_.isArray(value)){
              _.forEach(value, function(v, i){
                _.assignIn(result, $api._params(v, key+'['+i+']'));
              });
            } else if(_.isObject(value)){
              _.assignIn(result, $api._params(value, key));
            } else {
              result[key] = value;
            }
          });
        }
        return result;
      },
      _request:function(params) {
        var result = [];
        if (_.isObject(params)) {
          _.forEach(this._params(params), function (value, key) {
            result.push([key, encodeURIComponent(value)].join('='));
          });
        }
        return result.join('&');
      },
      _headers:function(method, headers){
        if(_.indexOf(['post', 'put', 'delete', 'options'], method) >= 0){
          _.assignIn(headers, {
            'Content-Type':'application/x-www-form-urlencoded'
          });
        }
        if(_.isString($Storage.get('userToken'))){
          _.assignIn(headers, {
            'Authorization':'Bearer ' + $Storage.get('userToken')
          })
        }
        return headers;
      },
      _response:function(response){
        if(response){
          return _.assignIn(response.data, {
            paging:response.paging
          });
        }
      },
      _errors:function(errors){
        if(!_.isObject(errors)){
          console.error('API:', errors);
          $mdToast.show(
            $mdToast.simple()
              .textContent(errors)
              .position('bottom right')
              .hideDelay(3000)
          );
        } else {
          _.forEach(errors, function(error){
            $api._errors(error);
          });
        }
      }
    });

    $api.setErrorInterceptor(function(response, deferred, responseHandler){
      if(response.data && response.data.status){
        if(response.data.status.success === false){
          $api._errors(response.data.errors);
        } else {
          responseHandler(response);
          return false;
        }
      }
    }).addResponseInterceptor(function(response){
      return $api._response(response);
    }).addFullRequestInterceptor(function(element, method, route, url, headers, params){
      return {
        headers:$api._headers(method, headers),
        element:$api._request(element),
        params:$api._params(params)
      };
    });

    return $api;
  }
]);