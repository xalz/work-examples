app.service('$Storage', [
  '$localStorage',
  function($localStorage){

    var $storage = {
      set:function(name, value){
        return $localStorage[name] = value;
      },
      get:function(name){
        return $localStorage[name];
      },
      remove:function(name) {
        return delete $localStorage[name];
      }
    };

    return $storage;
  }
]);