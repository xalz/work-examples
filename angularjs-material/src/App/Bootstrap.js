app.config([
  '$stateProvider', '$urlRouterProvider', '$mdThemingProvider',
  'uiGmapGoogleMapApiProvider', '$locationProvider',
  function(
      $stateProvider, $urlRouterProvider, $mdThemingProvider,
      uiGmapGoogleMapApiProvider, $locationProvider
  ){

    $locationProvider.html5Mode(config.html5Mode);
    uiGmapGoogleMapApiProvider.configure(config.map);
    _.forEach(config.states, function(state, key){
      $stateProvider.state(key, state);
    });
    _.forEach(config.themes, function(styles, key){
      switch (key){
        case 'default':
          var theme = $mdThemingProvider.theme(key);
          _.forEach(styles, function(style, key){
            theme[key](style.name, style.hues);
          });
          break;
        default:
          $mdThemingProvider.definePalette(key, styles);
          break;
      }
    });
  }
]);