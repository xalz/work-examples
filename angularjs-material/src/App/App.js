var app = angular.module('app', [
  'ngAnimate', 'ngAria','restangular', 'nemLogging',
  'ui.router', 'ngMaterial', 'uiGmapgoogle-maps',
  'ngMask', 'ngStorage'
]);