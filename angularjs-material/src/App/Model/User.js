app.factory('$User', [
  '$rootScope', '$API', '$Storage', '$q', '$Model',
  function($rootScope, $API, $Storage, $q, $Model){
    return function(){

      var $api = $API.all('users');

      var model = {
        is:{
          access:function(){},
          auth:function(){
            return _.isNumber(model.id);
          }
        },
        current:function(){
          var deferred = $q.defer();
          $api.withHttpConfig({
            cache: false
          }).customGET().then(function(user){
            _.assignIn(model, user);
            deferred.resolve(user);
          });
          return deferred.promise;
        },
        watch:function(callback){
          $rootScope.$watch(function(){
            return $Storage.get('userToken');
          }, function(token){
            if(_.isFunction(callback)){
              callback(token);
            }
            if(_.isString(token)){
              model.current();
            }
          });
        },
        signIn:function(){
          var deferred = $q.defer();
          $api.one('signIn').customPOST(model).then(function(user){
            _.assignIn(model, user);
            $Storage.set('userToken', user.token);
            deferred.resolve(user);
          });
          return deferred.promise;
        },
        signUp:function(){
          var deferred = $q.defer();
          $api.one('signUp').customPOST(model).then(function(user){
            _.assignIn(model, user);
            $Storage.set('userToken', user.token);
            deferred.resolve(user);
          });
          return deferred.promise;
        },
        signOut:function(){
          var deferred = $q.defer();
          $api.one('signOut').then(function(){
            $Storage.remove('userToken');
            deferred.resolve();
          });
          return deferred.promise;
        }
      };

      return _.assignIn(model, $Model);
    }
  }
]);