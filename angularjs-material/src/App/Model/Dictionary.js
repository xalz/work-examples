app.factory('$Dictionary', [
  '$rootScope', '$Model', '$q', '$API',
  function($rootScope, $Model, $q, $API){
    return function(){

      var $api = $API.all('dictionaries');

      var model = {

        list:function(filter, sort){
          var deferred = $q.defer();
          $api.customGET().then(function(items){
            _.forEach(items, function(item){
              item.alias = model._alias(item)
            });
            items = filter ? _.filter(items, filter) : items;
            items = sort ? _.sortBy(items, sort) : items;
            _.assignIn(model, items);
            deferred.resolve(items);
          });
          return deferred.promise;
        },

        code:function(code){
          var deferred = $q.defer();
          $api.all('code').customGET(code).then(function(items){
            _.assignIn(model, items);
            deferred.resolve(items);
          });
          return deferred.promise;
        }

      };

      return _.assignIn(model, $Model);
    }
  }
]);