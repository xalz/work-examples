app.factory('$Photo', [
  '$rootScope', '$q', '$API', '$Model',
  function($rootScope, $q, $API, $Model){
    return function(){

      var $api = $API.all('photos');
      var model = {
        resize:function(id, width, height){
          var deferred = $q.defer();
          $api.one('resize', id).customPUT({
            width:width,
            height:height
          }).then(function(item){
            _.assignIn(model, item);
            deferred.resolve(item);
          });
          return deferred.promise;
        }
      };

      return _.assignIn(model, $Model);
    }
  }
]);