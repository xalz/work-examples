app.service('$Offer', [
  '$rootScope', '$API', '$q', '$Model',
  function($rootScope, $API, $q, $Model){
    return function(){

      var $api = $API.all('offers');

      var model = {

        _aliases:['type', 'location', 'rooms'],

        group:function(type_id){
          var deferred = $q.defer();
          $api.all('group').customGET(type_id).then(function(items){
            _.assignIn(model, items);
            deferred.resolve(items);
          });
          return deferred.promise;
        },
        view:function(offer_id){
          var deferred = $q.defer();
          $api.all('view').customGET(offer_id).then(function(item){
            model.alias(item);
            _.assignIn(model, item);
            deferred.resolve(item);
          });
          return deferred.promise;
        },
        counts:function(params){
          var deferred = $q.defer();
          $api.all('counts').customGET(null, params).then(function(items){
            _.assignIn(model, items);
            deferred.resolve(items);
          });
          return deferred.promise;
        },
        list:function(params){
          var deferred = $q.defer();
          $api.customGET(null, params).then(function(items){
            _.forEach(items, function(item) {
              model.alias(item);
            });
            _.assignIn(model, items);
            deferred.resolve(items);
          });
          return deferred.promise;
        },
        points:function(params){
          var deferred = $q.defer();
          $api.all('points').customGET(null, params).then(function(items){
            _.assignIn(model, items);
            deferred.resolve(items);
          });
          return deferred.promise;
        },
        fields:function(){
          var deferred = $q.defer();
          $api.all('fields').customGET().then(function(items){
            _.assignIn(model, items);
            deferred.resolve(items);
          });
          return deferred.promise;
        }
      };

      return _.assignIn(model, $Model);

    }
  }
]);