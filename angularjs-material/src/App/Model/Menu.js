app.factory('$Menu', [
  '$rootScope', '$q', '$API', '$state', '$Model',
  function($rootScope, $q, $API, $state, $Model){
    return function(){
      var __hard = [
        {
          name:'Меню',
          menu:[
            {
              name:'На Главную',
              description:'',
              keywords:'',
              href:'Main.Index',
              access:'Guest'
            },
            {
              name:'Все Объявления',
              description:'',
              keywords:'',
              href:'Main.OfferIndex',
              access:'Guest'
            }
          ]
        },
        {
          name:'Личный кабинет',
          menu:[
            {
              name:'Мои объявления',
              description:'',
              keywords:'',
              href:'main.cabinet.offers',
              access:'User'
            }
          ]
        },
        {
          name:'Управление сайтом',
          menu:[
            {
              name:'Все пользователи',
              description:'',
              keywords:'',
              href:'main.admin.users',
              access:'Moderator'
            }
          ]

        }
      ];

      var model = {
        list:function(){
          var deferred = $q.defer();

          _.assignIn(model, __hard);
          deferred.resolve(__hard);

          return deferred.promise;
        }
      };

      return _.assignIn(model, $Model);
    }
  }
]);