app.factory('$Page', [
  '$rootScope', '$API', '$q', '$Model',
  function($rootScope, $API, $q, $Model){
    return function(){

      var $api = $API.all('pages');

      var __hard = [
        {
          name:'Меню',
          menu:[
            {
              name:'Главная',
              description:'',
              keywords:'',
              state:'Main.Index',
              access:'Guest'
            },
            {
              name:'Все Объявления',
              description:'',
              keywords:'',
              href:'Main.OfferIndex',
              access:'Guest'
            }
          ]
        },
        {
          name:'Личный кабинет',
          menu:[
            {
              name:'Мои объявления',
              description:'',
              keywords:'',
              href:'main.cabinet.offers',
              access:'User'
            }
          ]
        },
        {
          name:'Управление сайтом',
          menu:[
            {
              name:'Все пользователи',
              description:'',
              keywords:'',
              href:'main.admin.users',
              access:'Moderator'
            }
          ]

        }
      ];


      var _hard_pages = [
        {
          name:'Главная',
          description:'',
          keywords:'',
          state:'Main.Index',
          access:'Guest',
          block:[
            {
              
            }
          ]
        }
      ];

      var model = {
        list:function(){
          var deferred = $q.defer();
          _.assignIn(model, __hard);
          deferred.resolve(__hard);
          /*$api.customGET(null).then(function(items){
            _.assignIn(model, items);
            deferred.resolve(items);
          });*/
          return deferred.promise;
        },
        view:function(page_id){
          var deferred = $q.defer();
          $api.all('view').customGET(page_id).then(function(item){
            _.assignIn(model, item);
            deferred.resolve(item);
          });
          return deferred.promise;
        }
      };

      return _.assignIn(model, $Model);
    }
  }
]);