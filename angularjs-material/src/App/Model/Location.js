app.factory('$Location', [
  '$rootScope', '$q', '$API', '$Model',
  function($rootScope, $q, $API, $Model){
    return function(){

      var $api = $API.all('locations');

      var model = {
        current:function() {
          var deferred = $q.defer();
          $api.all('current').withHttpConfig({
            cache: false
          }).customGET().then(function(item){
            _.assignIn(model, item);
            deferred.resolve(item);
          });
          return deferred.promise;
        },
        regions:function(){
          var deferred = $q.defer();
          $api.all('regions').getList().then(function(items){
            _.assignIn(model, items);
            deferred.resolve(items);
          });
          return deferred.promise;
        },
        cities:function(region_id){
          var deferred = $q.defer();
          $api.all('cities').all(region_id).getList().then(function(items){
            _.assignIn(model, items);
            deferred.resolve(items);
          });
          return deferred.promise;
        }
      };

      return _.assignIn(model, $Model);
    }
  }
]);