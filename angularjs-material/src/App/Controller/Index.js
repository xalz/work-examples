app.controller('IndexCtrl', [
  '$scope', '$Offer', '$Dictionary',
  function($scope, $Offer, $Dictionary){

    $scope.change_type = function(type_id){
      $scope.offer_groups = new $Offer();
      $scope.offer_groups.group(type_id);
    };

    $scope.offer_types = new $Dictionary();
    $scope.offer_types.list({ code: 'offer_type' });

    $scope.change_type();
    $scope.offer_type_id = 0;

  }
]);