app.controller('DashboardAuthCtrl', [
  '$scope', '$Storage', '$state',
  function($scope, $Storage, $state){

    $scope.tab = 'signIn';

    $scope.submit = {
      signIn:function(){
        $scope.$User.signIn().then(function(){
          $state.go('Main.DashboardIndex');
        });
      },
      signUp:function(){
        $scope.$User.signUp().then(function(){
          $scope.tab = 'signIn';
        });
      }
    };

    $scope.methods = [
      {
        code:'phone',
        limit:true,
        mask:'+7(999)999-9999',
        button:'Выслать СМС с паролем',
        complete:'На ваш номер было высланно СМС с паролем'
      },
      {
        code:'email',
        limit:false,
        mask:'*',
        button:'Выслать письмо с паролем',
        complete:'На ваш почтовый ящик было высланно письмо с паролем'
      }
    ];

    $scope.$watch(function(){
      return $scope.$User.username
    }, function(username){
      if(username && username.match(/^(\d|\+|\(|\)|-)+$/)){
        $scope.method = _.find($scope.methods, {
          code:'phone'
        });
      } else {
        $scope.method = _.find($scope.methods, {
          code:'email'
        });
      }
    });

  }
]);