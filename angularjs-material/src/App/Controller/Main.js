app.controller('MainCtrl', [
  '$scope', '$User', '$Page', '$mdMedia', '$state', '$mdSidenav',
  function($scope, $User, $Page, $mdMedia, $state, $mdSidenav){

    $scope.$User = new $User();
    $scope.$Menu = new $Page();
    $scope.$Page = new $Page();
    $scope.$mdMedia = $mdMedia;
    $scope.$mdSidenav = $mdSidenav;
    $scope.$Moment = moment;


    $scope.toolbar_button = {
      add:function(){
        $state.go('Main.OfferAdd');
      },
      auth:function(){
        $state.go('Main.DashboardAuth');
      },
      exit:function(){
        $scope.$User.signOut().then(function(){
          $state.go('Main.Index');
        });
      }
    };

    $scope.$User.watch(function(){
      $scope.$Menu.list();
    });

  }
]);