app.controller('OfferEditCtrl', [
  '$scope', '$state', '$Offer',
  function($scope, $state, $Offer){

    $scope.fields = new $Offer();
    $scope.fields.fields();

    $scope.offer = new $Offer();
    $scope.offer.view($state.params.id);

    $scope.$watch(function(){
      return $scope.offer.id;
    }, function(){
      console.log('$watch', $scope.offer)
    })

  }
]);