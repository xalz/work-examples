app.controller('OfferViewCtrl', [
  '$scope', '$Offer', '$state',
  function($scope, $Offer, $state){

    $scope.offer = new $Offer();
    $scope.offer.view($state.params.id);

  }
]);