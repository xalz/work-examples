app.controller('OfferIndexCtrl', [
  '$scope', '$Offer', '$state', '$Dialog',
  function($scope, $Offer, $state, $Dialog){

    $scope.filter = {
      region:$state.params.region,
      city:$state.params.city,
      page:$state.params.page,
      period:$state.params.period,
      tags:$state.params.tags,
      rooms:$state.params.rooms,
      type:$state.params.type,
      price:$state.params.price,
      floor:$state.params.floor,
      area:$state.params.area,
      geometry:$state.params.geometry
    };

    $scope.$watchCollection(function(){
      return [$scope.filter.page];
    }, function(){
      $state.go('Main.OfferIndex', $scope.filter);
    });

    $scope.offers = new $Offer();
    $scope.offers.list($scope.filter);

  }
]);