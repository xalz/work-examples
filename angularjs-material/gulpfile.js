var gulp = require('gulp');
var $ = require('gulp-load-plugins')({
    pattern: [
        'gulp-*','run-sequence','browser-sync',
        'browser-sync-spa','yamljs','main-bower-files',
        'path','extend'
    ]
});
var wiredep = require('wiredep').stream;

/////////////////// CONFIG ////////////////////

var config = getAppConfigYaml();

////////////////// DEFAULT TASK ////////////////////

gulp.task('default', ['serve'], function () {
    $.runSequence('watch:all','browser-sync:serve');
});

///////////////// ENV VARS /////////////////

var ENV = getENV();


//////////////// WATCH //////////////////////

gulp.task('watch:all', ['index:watch','scripts:watch','styles:watch','views:watch','assets:watch']);


var debounce;

function isDebounce() {
    if (debounce) {
        return true;
    } else {
        debounce = true;
        return false;
    }
}

function reload() {
    debounce = false;
    $.browserSync.reload();
}

function errorHandler(error){
    console.log(error);
}

gulp.task('index:watch', function(){
    $.watch([
        'src/index.html',
        'bower.json',
        'src/View/**/*.html'
    ], function(e){
        $.runSequence('inject',reload);
    });
});

gulp.task('scripts:watch', function(){
    $.watch([
        'src/App/**/*.js'
    ], function(file){
        if (isDebounce()) {
            return;
        }
        if (file.event==='change') {
            $.runSequence('scripts:serve',reload);
        } else {
            $.runSequence('scripts:serve', 'inject:scripts', reload);
        }
    });
});

gulp.task('styles:watch', function(){

    $.watch([
        'src/scss/**/*.scss'
    ], function(file){
        if (isDebounce()) {
            return;
        }
        if (file.event==='change') {
            $.runSequence('styles:serve',reload);
        } else {
            $.runSequence('styles:serve', 'inject:styles', reload);
        }
    });
});

gulp.task('views:watch',function(){
    $.watch([
        'src/View/**/*.html'
    ], function(file){
        if (isDebounce()) {
            return;
        }
        $.runSequence('views:serve', reload);
    });
});

gulp.task('assets:watch',function(){
    $.watch([
        'src/Assets/**/*.*'
    ], function(file){
        if (isDebounce()) {
            return;
        }
        $.runSequence('assets:serve-non-images', reload);
    });
    gulp.watch([
        'src/Assets/Images/**/*.*'
    ], function(file){
        if (isDebounce()) {
            return;
        }
        $.runSequence('assets:serve-images', reload);
    });
});



///////////////// INJECT //////////////////

gulp.task('inject',function(done){
    $.runSequence('inject:index','inject:bower-dependencies','inject:styles', 'inject:scripts',done);
});

var injectOptions = {
    ignorePath: [
        '.tmp/serve'
    ],
    addRootSlash: true
};

var cssInject = $.extend({},injectOptions,{
    transform: function(filePath){
        var media = '', fileName = $.path.basename(filePath);
        if (config.mediaCss && config.mediaCss[fileName]) {
            media = 'media="'+config.mediaCss[fileName]+'" ';
        }
        return '<link role="app" rel="stylesheet" '+media+'href="'+filePath+'" />';
    }
});

gulp.task('inject:bower-dependencies',function(done){
    var bowerJson;
    try {
        bowerJson = require('./bower.json');
    } catch (e) {}
    if (bowerJson) {
        return gulp
            .src('.tmp/serve/index.html')
            .pipe(wiredep({
                ignorePath: '../../bower_components/',
                fileTypes: {
                    html: {
                        block: /(([ \t]*)<!--\s*bower:*(\S*)\s*-->)(\n|\r|.)*?(<!--\s*endbower\s*-->)/gi,
                        detect: {
                            js: /<script.*src=['"]([^'"]+)/gi,
                            css: /<link.*href=['"]([^'"]+)/gi
                        },
                        replace: {
                            js: '<script role="bower_components" src="/{{filePath}}"></script>',
                            css: '<link role="bower_components" rel="stylesheet" href="/{{filePath}}" />'
                        }
                    }
                }
            }))
            .pipe(gulp.dest('.tmp/serve'));
    } else {
        return done();
    }
});

gulp.task('watch:bower-dependencies',function(){
    $.watch([
        './bower.json'
    ],function(){
        $.runSequence('inject',$.browserSync.reload);
    });
});

gulp.task('inject:styles',function(){

    return gulp
        .src('.tmp/serve/index.html')
        .pipe($.inject(gulp.src('.tmp/serve/**/*.css',{ read: false }),cssInject))
        .pipe(gulp.dest('.tmp/serve'))
        ;
});

gulp.task('inject:scripts',function(){
    return gulp
        .src('.tmp/serve/index.html')
        .pipe($.inject(gulp
                .src('.tmp/serve/**/*.js', {read: true})
            //.pipe($.if(config.ngApp,$.angularFilesort()))
            ,
            injectOptions
        ))
        .pipe(gulp.dest('.tmp/serve'))
        ;
});

gulp.task('inject:index',function(){
    return gulp
        .src('src/index.html')
        .pipe($.replace('</title>',''
            +'</title>\n'
            +'<!-- buildbower:css /App/vendors.css -->\n'
            +'  <!-- bower:css -->\n'
            +'  <!-- endbower -->\n'
            +'<!-- endbuildbower -->\n'
            +'<!-- inject:css -->\n'
            +'<!-- endinject -->\n'
        ))
        .pipe($.replace('</body>',''
            +'\n<!-- buildbower:js /App/vendors.js -->\n'
            +'  <!-- bower:js -->\n'
            +'  <!-- endbower -->\n'
            +'<!-- endbuildbower -->\n'
            +'<!-- build:js /App/scripts.js -->\n'
            +'  <!-- inject:js -->\n'
            +'  <!-- endinject -->\n'
            +(config.ngApp ? '  <!-- inject:partials -->\n  <!-- endinject -->\n' : '')
            +'<!-- endbuild -->\n'
            +'</body>'
        ))
        .pipe($.fileInclude())
        .pipe($.template({'ENV':ENV}))
        .pipe(gulp.dest('.tmp/serve'))
        ;

});



///////////// SCRIPTS ////////////////

(function(){

    gulp.task('scripts:serve', ['scripts:compile'], function(done){
        $.runSequence('scripts:insert-ng-modules',done);
    });

    gulp.task('scripts:compile', ['scripts:clean'], function(done){
        return gulp
            .src('src/App/**/*.js')
            .pipe($.plumber({
                errorHandler:errorHandler
            }))
            .pipe($.replaceTask({
                usePrefix: false,
                patterns:[{json: {
                    '$ENV': ENV
                }}]
            }))
            //.pipe($.jslint())
            .pipe(gulp.dest('.tmp/serve/App'))
            ;
    });

    gulp.task('scripts:insert-ng-modules', ['scripts:parse-ng-modules'], function(done){
        if (!config.ngApp)
            done();
        else
            return gulp
                .src('.tmp/serve/App/**/*.js')
                .pipe($.replace('/* ngModules */',parseNgModules.getList()))
                .pipe(gulp.dest('.tmp/serve/App'))
                ;
    });

    gulp.task('scripts:parse-ng-modules', function(done){
        if (!config.ngApp)
            done();
        else
            $.runSequence(
                'scripts:parse-bower-ng-modules',
                'scripts:parse-app-ng-modules',
                done
            );
    });

    gulp.task('scripts:parse-bower-ng-modules', function(done){
        if (!config.ngApp)
            done();
        else {
            var src;
            try {
                src = $.mainBowerFiles();
            } catch(e) {}
            src = src||[];
            return gulp
                .src(src)
                .pipe($.insert.transform(parseNgModules));
        }
    });

    gulp.task('scripts:parse-app-ng-modules', function(done){
        if (!config.ngApp)
            done();
        else
            return gulp
                .src(['src/App/**/*.js'])
                .pipe($.insert.transform(parseNgModules));
    });

    gulp.task('scripts:clean', function(){
        return gulp.src('.tmp/serve/App', {read: false})
            .pipe($.plumber({
                errorHandler:errorHandler
            }))
            .pipe($.rimraf({force:true}));
    });

})();

////////////// STYLES /////////////////

gulp.task('styles:serve', ['styles:clean'], function(){
    return gulp.src('src/scss/**/*.scss')
        .pipe($.sass())
        .on('error', $.sass.logError)
        .pipe(gulp.dest('.tmp/serve/Css/'))
        .pipe(gulp.dest('.tmp/serve/Css/'))

});

gulp.task('styles:clean', function(done){
    return gulp.src('.tmp/serve/Css', {read: false})
        .pipe($.plumber({
            errorHandler:errorHandler
        }))
        .pipe($.rimraf({force: true}));
});

/////////////// VIEWS //////////////////

gulp.task('views:serve', ['views:clean'],function(){
    return gulp
        .src('src/View/**/*.html')
        .pipe($.plumber({
            errorHandler:errorHandler
        }))
        .pipe($.fileInclude())
        .pipe($.template({'ENV':ENV}))
        .pipe(gulp.dest('.tmp/serve/View'))
        ;
});

gulp.task('views:clean', function(done){
    return gulp.src('.tmp/serve/View', {read: false})
        .pipe($.plumber({
            errorHandler:errorHandler
        }))
        .pipe($.rimraf({force: true}));
});

////////////// ASSETS ////////////////

gulp.task('assets:serve',['assets:serve-non-images','assets:serve-images']);

gulp.task('assets:serve-non-images', ['assets:serve-clean-non-images'], function() {
    return gulp
        .src(['src/Assets/**/*.*', '!app/Assets/Images/**/*.*'])
        .pipe(gulp.dest('.tmp/serve/Assets'))
        ;
});

gulp.task('assets:serve-images', ['assets:serve-clean-images'], function(){
    return gulp
        .src(['src/Assets/Images/**/*.*'])
        .pipe($.plumber({
            errorHandler:errorHandler
        }))
        .pipe($.imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest('.tmp/serve/Assets/Images'))
        ;
});

gulp.task('assets:serve-clean-non-images', function(done){
    return gulp.src(
        [
            '.tmp/serve/Assets',
            '!.tmp/serve/Assets/Images'
        ], {read: false}
    )
        .pipe($.plumber({
            errorHandler:errorHandler
        }))
        .pipe($.rimraf({force: true}));
});

gulp.task('assets:serve-clean-images', function(done){
    return gulp.src('.tmp/serve/Assets/Images', {read: false})
        .pipe($.plumber({
            errorHandler:errorHandler
        }))
        .pipe($.rimraf({force: true}));
});



///////////////// SERVE  //////////////////

gulp.task('serve',['assets:serve','scripts:serve','styles:serve','views:serve','inject'], function(done){
    done();
});

////////////// BROWSER-SYNC ///////////////

gulp.task('browser-sync:serve', ['inject'], function (done) {
    browserSyncInit([
        'bower_components',
        '.tmp/serve'
    ],{
        done:$.browserSync.reload,
        open:true
    });
});


///////////////// BUILD ////////////////////

gulp.task('build', function () {
    $.runSequence(
        [
            'serve',
            'build:ng-template-cache'
        ],
        'build:sources',
        'build:clean-build-tmp',
        'build:copy-assets',
        function () {

            gulp
                .src('dist/**/*.*')
                .pipe($.size())
                .pipe($.size({gzip:true}))
            ;
            browserSyncInit('dist',{
                done:$.browserSync.reload,
                open:true
            });
        }
    );
});

gulp.task('build:sources',function(done){
    $.runSequence(
        'build:clean-dist',
        'build:app-sources',
        'build:bower-sources',
        'build:copy-views',
        done
    );
});

gulp.task('build:copy-views',function(done){
    if (config.ngApp)
        done();
    else
        return gulp
            .src('.tmp/serve/View/**/*.html')
            .pipe($.minifyHtml({
                empty: true,
                spare: true,
                quotes: true,
                conditionals: true
            }))
            .pipe(gulp.dest('dist/View'))
            ;
});

gulp.task('build:app-sources', ['inject'], function(){
    return gulp
        .src('.tmp/serve/index.html')
        .pipe($.plumber({
            errorHandler:errorHandler
        }))
        .pipe($.replace(
            /<link role="app"(.*?)href="(.*?)"(.*?)>/g,
            '<!-- build:css $2--><link$1href="$2"$3><!-- endbuild -->')
        )

        .pipe($.if(config.ngApp,$.inject(
            gulp.src('.tmp/serve/build-tmp/templateCacheHtml.js',{read:false}),{
                starttag: '<!-- inject:partials -->',
                addRootSlash: false,
                ignorePath: '.tmp/serve'
            }
        )))

        .pipe($.usemin({
            js: [
                function () {
                    return $.if(config.ngApp,$.angularFilesort());
                },
                function(){
                    return $.if(config.ngApp,$.ngAnnotate());
                },
                function(){
                    return $.stripComments({safe:false});
                },
                $.stripDebug,
               /* function(){
                    return $.uglify({ preserveComments: function(){ return false; } });
                },*/
                $.rev
            ],
            css: [
                function(){
                    return $.stripComments({safe:false});
                },
                $.csso,
                $.rev
            ]
        }))
        .pipe(gulp.dest('dist'))
        ;
});

gulp.task('build:bower-sources', function(){
    var htmlFilter = $.filter('*.html',{restore: true});
    var jsFilter = $.filter(['**/*.js'],{restore: true});
    var cssFilter = $.filter((!config.mediaCss ? ['**/*.css'] : []),{restore: true});
    var assets = $.useref.assets({searchPath: 'bower_components'});
    return gulp
        .src(['dist/index.html'])
        .pipe($.eol())
        .pipe($.plumber())
        .pipe($.replace('buildbower','build'))
        .pipe(assets)
        .pipe($.rev())

        .pipe(jsFilter)
        .pipe($.stripDebug())
        .pipe($.uglify({ preserveComments: function(){ return false; } }))
        .pipe(jsFilter.restore)
        .pipe(cssFilter)
        .pipe($.stripComments({safe:false}))
        .pipe($.csso())
        .pipe(cssFilter.restore)

        .pipe(assets.restore())

        .pipe($.useref())
        .pipe($.revReplace())
        .pipe(htmlFilter)
        .pipe($.minifyHtml({
             empty: true,
             spare: true,
             quotes: true,
             conditionals: true
         }))
        .pipe(htmlFilter.restore)

        .pipe(gulp.dest('dist'))
        ;
});

gulp.task('build:copy-assets',function(){
    return gulp.src(['src/Assets/**/*.*'])
        .pipe(gulp.dest('dist/Assets'));
});

gulp.task('build:ng-template-cache', function (done) {
    if (!config.ngApp)
        done();
    else
        return gulp.src(['src/View/**/*.html'])
            .pipe($.minifyHtml({
                empty: true,
                spare: true,
                quotes: true
            }))
            .pipe($.angularTemplatecache('templateCacheHtml.js', {
                templateHeader: config.ngApp + '.run(["$templateCache", function($templateCache) {',
                module: config.ngApp,
                root: '/View'
            }))
            .pipe(gulp.dest('.tmp/serve/build-tmp'));
});

gulp.task('build:clean-build-tmp',function(done){
    return gulp.src('.tmp/serve/build-tmp', {read: false})
        .pipe($.plumber({
            errorHandler:errorHandler
        }))
        .pipe($.rimraf({force: true}));
});

gulp.task('build:clean-dist', function (done) {
    return gulp.src('dist', {read: false})
        .pipe($.plumber({
            errorHandler:errorHandler
        }))
        .pipe($.rimraf({force: true}));
});


///////////////// HELPERS ////////////////////

function browserSyncInit(baseDir,options) {
    options = options||{};
    if (config.ngApp) {
        $.browserSync.use($.browserSyncSpa({
            selector: '[ng-app]'
        }));
    }
    $.browserSync.instance = $.browserSync.init({
        startPath: '/',
        server: {
            baseDir: baseDir
        },
        port: config.port||3000,
        browser: 'default',
        notify: false,
        open: options.open!=undefined ? options.open : false,
        reloadOnRestart: options.reloadOnRestart!=undefined ? options.reloadOnRestart : true
    },options.done ? options.done : function(){});
}

function getENV() {
    var all, envConfig;
    try {
        all = $.yamljs.load('src/env/all.yml');
        envConfig = $.yamljs.load('src/env/' + process.env.NODE_ENV + '.yml');
    } catch (e) {}
    return mergeObjects(all||{},envConfig||{});
}

function mergeObjects(obj, ext){
    var merged = $.extend({},obj);
    for (var prop in ext) {
        if (ext.hasOwnProperty(prop)) {
            merged[prop] =
                (
                    Object.prototype.toString.call(obj[prop])=='[object Object]'
                    ||
                    Object.prototype.toString.call(obj[prop])=='[object Function]'
                )
                    ? mergeObjects(obj[prop],ext[prop])
                    : ext[prop];
        }
    }
    return merged;
}

function getAppConfigYaml() {
    var appconfig;
    try {
        appconfig = $.yamljs.load('src/Config.yml');
    } catch (e) {}
    return appconfig||{};
}

function parseNgModules(code) {
    parseNgModules.list = parseNgModules.list || [];
    var find = code.replace(/(\/\*([\s\S]*?)\*\/)|(\/\/(.*)$)/gm,'')
        .replace(/angular\s+\.module/gm, 'angular.module')
        .match(/angular.module\(([a-zA-Z0-9\-\.'"]+),(|\s+)\[(|[\sa-zA-Z0-9\-\.'",]+)(|\s+)\]/g);
    if (find) {
        var modules = {};
        for (var i=0,l=find.length;i<l;i++) {
            var module = find[i].replace('angular.module(','').replace(/\s+/g,'').replace(']','').split(',[');
            var moduleName = module[0].replace(/('|")/g,'');
            var moduleDeps = module[1].replace(/('|")/g,'');
            modules[moduleName] = moduleDeps;
        }
        var submodules = [];
        var deps = [];
        for (var submoduleName in modules) {
            deps.push(modules[submoduleName]);
            submodules.push(submoduleName);
        }
        deps = deps.join(',').split(',');
        for (var ii=0,ll=submodules.length;ii<ll;ii++) {
            if (submodules[ii]!=='ngLocale' && submodules[ii]!==config.ngApp && deps.indexOf(submodules[ii])<0) {
                parseNgModules.list.push(submodules[ii]);
            }
        }
    }
    return code;
}
parseNgModules.getList = function(){
    parseNgModules.list = parseNgModules.list||[];
    var list = '';
    if (parseNgModules.list.length) {
        list = '\n\t\'' + parseNgModules.list.join('\',\n\t\'') + '\'\n';
        parseNgModules.list = [];
    }
    return list;
};
