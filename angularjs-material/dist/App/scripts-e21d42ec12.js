var app = angular.module('app', [
  'ngAnimate', 'ngAria','restangular', 'nemLogging',
  'ui.router', 'ngMaterial', 'uiGmapgoogle-maps',
  'ngMask', 'ngStorage'
]);
app.config([
  '$stateProvider', '$urlRouterProvider', '$mdThemingProvider',
  'uiGmapGoogleMapApiProvider', '$locationProvider',
  function(
      $stateProvider, $urlRouterProvider, $mdThemingProvider,
      uiGmapGoogleMapApiProvider, $locationProvider
  ){

    $locationProvider.html5Mode(config.html5Mode);
    uiGmapGoogleMapApiProvider.configure(config.map);
    _.forEach(config.states, function(state, key){
      $stateProvider.state(key, state);
    });
    _.forEach(config.themes, function(styles, key){
      switch (key){
        case 'default':
          var theme = $mdThemingProvider.theme(key);
          _.forEach(styles, function(style, key){
            theme[key](style.name, style.hues);
          });
          break;
        default:
          $mdThemingProvider.definePalette(key, styles);
          break;
      }
    });
  }
]);
var config = {

  api:{
    setBaseUrl:"http://api.4arenda.ru/",
    setRequestSuffix:".json",
    setDefaultHttpFields:{
      cache: true
    }
  },

  map:{
    key:'AIzaSyBYwcxh-a9_UH3HngMVxYRpY5DUTVkPzQ8',
    language:'ru',
    libraries:'geometry,drawing,places,visualization'
  },

  themes:{
    new4arenda:{
      '50': '#5dcef8',
      '100': '#13b8f5',
      '200': '#0894c8',
      '300': '#056184',
      '400': '#044c66',
      '500': '#3B3B3B',
      '600': '#02202c',
      '700': '#010b0e',
      '800': '#000000',
      '900': '#000000',
      'A100': '#5dcef8',
      'A200': '#13b8f5',
      'A400': '#044c66',
      'A700': '#010b0e',
      'contrastDefaultColor': 'light',
      'contrastDarkColors': '50 100 A100 A200'
    },
    default:{
      primaryPalette:{
        name:'new4arenda'
      },
      accentPalette:{
        name:'new4arenda'
      }
    }
  },

  html5Mode:{
    enabled:true,
    requireBase:false
  },

  states:{
    main:{
      url:'',
      abstract:true,
      templateUrl:"/View/Layout.html"
    },
    "main.index":{
      url:'/',
      views:{
        Content:{
          templateUrl:"/View/Pages/Index.html",
          controller:"IndexCtrl"
        }
      }
    },
    "main.offers":{
      url:'/offers/{page:int}?{rooms:int}&{region:int}&{city:int}&{tags:int}&{type:int}&{period:int}&{price:int}&{floor:int}&{area:int}&geometry',
      params:{
        page:{ value:1, dynamic:true },
        tags:{ value:[], array: true },
        rooms:{ value:[], array: true }
      },
      views:{
        Content:{
          templateUrl:"/View/Pages/Offer/Index.html",
          controller:"OfferIndexCtrl"
        }
      }
    },
    "main.offer":{
      url:'/offer/:id',
      views:{
        Content:{
          templateUrl:"/View/Pages/Offer/View.html",
          controller:"OfferViewCtrl"
        }
      }
    },
    "main.add":{
      url:"/offers/add",
      views:{
        Content:{
          templateUrl:"/View/Pages/Offer/Add/Index.html",
          controller:"OfferAddCtrl"
        }
      }
    }
  }
};
app.run(["$templateCache", function($templateCache) {$templateCache.put("/View/Layout.html","<md-toolbar layout=\"row\"><md-button ng-click=\"\" class=\"md-mini\" ng-if=\"!$mdMedia(\'gt-md\')\"><md-icon md-font-icon=\"mdi-menu\"></md-icon></md-button><md-button ui-sref=\"main.index()\"><img class=\"logo\" src=\"/Assets/Images/logo.png\"></md-button><div flex=\"\"></div><md-button ng-if=\"$mdMedia(\'gt-md\')\"><md-icon md-font-icon=\"mdi-bookmark-plus\"></md-icon>Добавить объявление</md-button><md-button ng-if=\"$mdMedia(\'gt-md\')\"><md-icon md-font-icon=\"mdi-key-variant\"></md-icon>Личный кабинет</md-button></md-toolbar><section layout=\"row\" flex=\"\"><md-sidenav class=\"md-sidenav-left\" md-component-id=\"left\" md-is-locked-open=\"$mdMedia(\'gt-md\')\" md-disable-backdrop=\"\" md-whiteframe=\"4\"><md-toolbar class=\"md-hue-2 account\"><div layout=\"row\" layout-align=\"center center\"><img class=\"md-avatar\" src=\"/Assets/Images/avatar.png\"><div flex=\"\"></div></div><div flex=\"\"></div><div class=\"md-body-2\">Firstname Lastname</div><div class=\"md-body-1\">email@domainname.com</div></md-toolbar><md-list><div ng-repeat=\"group in menu_groups\"><md-subheader class=\"md-no-sticky\">{{group.name}}</md-subheader><md-list-item ng-repeat=\"menu in group.menu\" ui-sref=\"{{menu.href}}\">{{menu.name}}</md-list-item></div></md-list><ng-view ui-view=\"LeftSidebar\"></ng-view></md-sidenav><md-content flex=\"\" layout-padding=\"\"><ng-view ui-view=\"Content\"></ng-view></md-content></section>");
$templateCache.put("/View/Pages/Index.html","<offer-filter></offer-filter><div layout=\"column\" layout-gt-md=\"row\"><md-card flex-gt-md=\"50\" flex-gt-xs=\"100\" flex-md=\"100\" layout=\"column\"><md-card-title><md-card-title-text><span class=\"md-headline\"><md-icon md-font-icon=\"\"></md-icon>Актуальные объявления</span> <span class=\"md-subhead\">Сгруппированные по городам</span></md-card-title-text></md-card-title><md-card-content><md-nav-bar md-selected-nav-item=\"offer_type_id\" nav-bar-aria-label=\"navigation links\"><md-nav-item md-nav-click=\"change_type()\" name=\"0\">Все</md-nav-item><md-nav-item md-nav-click=\"change_type(type.id)\" ng-repeat=\"type in offer_types\" name=\"{{type.id}}\">{{$DY.alias(type, 0)}}</md-nav-item></md-nav-bar><md-list><md-list-item class=\"md-2-line left\" ng-repeat=\"group in offer_groups\" ui-sref=\"main.offers({ region:group.region_id, city:group.city_id, type:group.type_id })\"><div class=\"md-label md-avatar\" layout=\"row\" layout-align=\"center center\">{{group.count}}</div><div class=\"md-list-item-text compact\"><h3>{{group.city_name}}</h3><p>{{group.region_name}}</p></div></md-list-item></md-list><div ng-show=\"offer_groups && offer_groups.length === 0\" layout=\"row\" layout-align=\"center center\">Ничего не найдено</div><preload model=\"offer_groups\" align=\"center center\" diameter=\"20\"></preload></md-card-content></md-card><md-card flex-gt-md=\"50\" flex-gt-xs=\"100\" flex-md=\"100\" layout=\"column\"><md-card-title><md-card-title-text><span class=\"md-headline\"><md-icon md-font-icon=\"\"></md-icon>Горячие объявления</span></md-card-title-text></md-card-title><md-card-content></md-card-content></md-card></div>");
$templateCache.put("/View/Widgets/Dialog.html","<md-dialog aria-label=\"{{title}}\"><md-toolbar><div class=\"md-toolbar-tools\"><h2>{{title}}</h2><span flex=\"\"></span><md-button class=\"md-icon-button\" ng-click=\"hide()\"><md-icon md-font-icon=\"mdi-close\" aria-label=\"Закрыть\"></md-icon></md-button></div></md-toolbar><md-dialog-content class=\"md-padding\"><ng-include ng-show=\"include\" src=\"include\"></ng-include><div ng-show=\"body\">{{body}}</div></md-dialog-content></md-dialog>");
$templateCache.put("/View/Widgets/Paginate.html","<div layout=\"row\" class=\"material-paging\" layout-align=\"{{align}}\" ng-show=\"showPaginate()\"><md-button class=\"md-primary\" ng-click=\"goFirst()\" ng-disabled=\"showFirst()\"><md-icon md-font-icon=\"mdi-chevron-double-left\"></md-icon>Первая</md-button><md-button class=\"md-primary\" ng-click=\"goPrev()\" ng-disabled=\"!showPrev()\"><md-icon md-font-icon=\"mdi-chevron-left\"></md-icon>Назад</md-button><md-button class=\"md-fab md-mini\" ng-repeat=\"page in pages\" ng-click=\"goPage(page)\" ng-class=\"{true: \'md-primary\', false: \'\'}[page === model.paging.current]\">{{page}}</md-button><md-button class=\"md-primary\" ng-click=\"goNext()\" ng-disabled=\"!showNext()\">Вперед<md-icon md-font-icon=\"mdi-chevron-right\"></md-icon></md-button><md-button class=\"md-primary\" ng-click=\"goLast()\" ng-disabled=\"showLast()\">Последняя<md-icon md-font-icon=\"mdi-chevron-double-right\"></md-icon></md-button></div>");
$templateCache.put("/View/Pages/Offer/Index.html","<offer-filter filter=\"filter\"></offer-filter><md-content ng-repeat=\"offer in offers\" layout-xs=\"column\" layout=\"row\"><md-card flex-xs=\"\" flex-gt-xs=\"30\" layout=\"column\"><photo src=\"offer.photos\" width=\"250\" height=\"200\" class=\"md-card-image\"></photo><md-card-actions layout=\"row\" layout-align=\"end center\"><div><md-icon md-font-icon=\"mdi-image\"></md-icon>{{offer.photos.length}}</div></md-card-actions></md-card><md-card flex-xs=\"\" flex-gt-xs=\"70\" layout=\"column\"><md-card-title><md-card-title-text><span class=\"md-headline\"><a ui-sref=\"main.offer({ id:offer.id })\" ui-sref-active=\"active\">{{$DY.alias(offer.type, 0)}} {{$DY.alias(offer.rooms, 1)}} в {{$DY.alias(offer.location, 5)}}, {{offer.location.street}}, {{offer.location.house}}</a></span> <span class=\"md-subhead\"><md-icon md-font-icon=\"mdi-calendar\"></md-icon>Опубликованно: {{$Moment(offer.created).fromNow()}}</span></md-card-title-text></md-card-title><md-card-content>{{offer.description}}<h3>{{offer.price}}</h3><md-chips ng-model=\"offer.tags\" readonly=\"true\"><md-chip-template><a ui-sref=\"main.offers({ tags:[$chip.id], page:1 })\" ui-sref-active=\"active\">{{$chip.text}}</a></md-chip-template></md-chips></md-card-content></md-card></md-content><preload model=\"offers\" align=\"center center\" diameter=\"20\"></preload><paginate align=\"center center\" length=\"7\" filter=\"filter\" model=\"offers\"></paginate>");
$templateCache.put("/View/Pages/Offer/View.html","<md-content layout-xs=\"column\" layout=\"row\"><md-card flex-xs=\"\" flex-gt-xs=\"90\" layout=\"column\"><md-card-title><md-card-title-text><span class=\"md-headline\">Описание {{offer.rooms.aliases[2]}} <span ng-if=\"offer.area\">, площадью {{offer.area}} м²</span></span> <span class=\"md-subhead\" layout=\"row\"><md-icon md-font-icon=\"mdi-calendar\"></md-icon>Опубликованно: {{$Moment(offer.created).fromNow()}}<div flex=\"\"></div><md-icon md-font-icon=\"mdi-eye\"></md-icon>Просмотров: {{offer.views}}</span></md-card-title-text></md-card-title><md-card-content>{{offer.description}}</md-card-content></md-card></md-content>");
$templateCache.put("/View/Widgets/Location/Select.html","<div layout-xs=\"column\" layout=\"row\"><md-input-container flex-xs=\"\" flex-gt-xs=\"50\" layout=\"column\"><label>Регион</label><md-select placeholder=\"{{empty || \'Выберите регион\'}}\" ng-model=\"region\" md-on-open=\"regions.get()\" ng-change=\"regions.set()\"><md-option ng-if=\"isEmpty()\" ng-value=\"\">{{empty}}</md-option><md-option ng-value=\"region.id\" ng-repeat=\"region in regions.data\">{{region.name}}</md-option></md-select></md-input-container><md-input-container flex-xs=\"\" flex-gt-xs=\"50\" layout=\"column\" ng-show=\"region\"><label>Город</label><md-select placeholder=\"{{empty || \'Выберите город\'}}\" ng-model=\"city\" md-on-open=\"cities.get()\" ng-change=\"cities.set()\"><md-option ng-if=\"isEmpty()\" ng-value=\"\">{{empty}}</md-option><md-option ng-value=\"city.id\" ng-repeat=\"city in cities.data\">{{city.name}}</md-option></md-select></md-input-container><md-button class=\"md-mini\" ng-click=\"current.trigger()\" aria-label=\"Определить\"><md-icon md-font-icon=\"mdi-map-marker\" ng-if=\"current.is(0)\"></md-icon><md-icon md-font-icon=\"mdi-close-circle-outline\" ng-if=\"current.is(1)\"></md-icon><div layout=\"row\" class=\"material-preload\" ng-if=\"current.is(2)\" layout-align=\"center center\"><md-progress-circular md-diameter=\"20\" md-mode=\"indeterminate\"></md-progress-circular></div></md-button></div>");
$templateCache.put("/View/Widgets/Offer/Filter.html","<div layout=\"column\" layout-gt-md=\"row\"><div flex-gt-md=\"30\" flex-gt-xs=\"100\" flex-md=\"100\" layout=\"column\"><md-card><md-card-title><md-card-title-text><span class=\"md-headline\"><md-icon md-font-icon=\"mdi-filter\"></md-icon>Поиск объявлений</span></md-card-title-text></md-card-title><md-card-content><location-select empty=\"Все\" region=\"filter.region\" geometry=\"filter.geometry\" city=\"filter.city\"></location-select><md-input-container flex-xs=\"\" flex-gt-xs=\"100\" layout=\"column\"><label>Тип объявления</label><md-select placeholder=\"Все\" ng-model=\"filter.type\"><md-option ng-value=\"\">Все</md-option><md-option ng-value=\"type.id\" ng-repeat=\"type in types\">{{type.aliases[0].name}}</md-option></md-select></md-input-container><md-input-container flex-xs=\"\" flex-gt-xs=\"100\" layout=\"column\"><label>Период</label><md-select placeholder=\"Все\" ng-model=\"filter.period\"><md-option ng-value=\"\">Все</md-option><md-option ng-value=\"period.id\" ng-repeat=\"period in periods\">{{period.name}}</md-option></md-select></md-input-container><md-input-container flex-xs=\"\" flex-gt-xs=\"100\" class=\"md-input-has-placeholder md-input-has-value\"><label>Количество комнат</label><md-checkbox ng-checked=\"room.checked()\" md-indeterminate=\"!room.checked()\" ng-click=\"room.toggle()\">Все</md-checkbox><md-checkbox ng-repeat=\"r in rooms\" ng-click=\"room.toggle(r.id, rooms.length)\" ng-checked=\"room.checked(r.id)\">{{r.aliases[3].name}}</md-checkbox></md-input-container></md-card-content><md-card-actions layout=\"row\" layout-align=\"end center\"><md-button ng-click=\"submit()\" class=\"md-raised md-primary\"><md-icon md-font-icon=\"mdi-filter\"></md-icon>Поиск ({{counts.count}})</md-button><md-button ng-click=\"details.trigger()\" ng-class=\"{true: \'md-primary\', false: \'\'}[details.isShow()]\">Расширеный поиск</md-button></md-card-actions></md-card></div><div flex-gt-md=\"70\" flex-gt-xs=\"100\" flex-md=\"100\" layout=\"column\" ng-show=\"details.isShow()\"><md-card flex=\"\"><md-card-title><md-card-title-text><span class=\"md-headline\"><md-icon md-font-icon=\"mdi-settings\"></md-icon>Расширеный поиск</span></md-card-title-text></md-card-title><md-card-content><md-input-container flex-xs=\"\" flex-gt-xs=\"50\" class=\"md-block md-input-has-placeholder md-input-has-value\"><label>Цена до <span ng-if=\"!slider.disabled(filter.price)\">{{filter.price}}</span> <span ng-if=\"slider.disabled(filter.price)\">[Все]</span></label><md-slider aria-label=\"price\" ng-model=\"filter.price\" ng-disabled=\"slider.disabled(counts.max_price)\" step=\"1000\" min=\"0\" max=\"{{counts.max_price}}\"></md-slider></md-input-container><md-input-container flex-xs=\"\" flex-gt-xs=\"50\" class=\"md-block md-input-has-placeholder md-input-has-value\"><label>Этаж <span ng-if=\"!slider.disabled(filter.floor)\">{{filter.floor}}</span> <span ng-if=\"slider.disabled(filter.floor)\">[Все]</span></label><md-slider aria-label=\"floor\" ng-model=\"filter.floor\" ng-disabled=\"slider.disabled(counts.max_floor)\" step=\"1\" min=\"0\" max=\"{{counts.max_floor}}\"></md-slider></md-input-container><md-input-container flex-xs=\"\" flex-gt-xs=\"50\" class=\"md-block md-input-has-placeholder md-input-has-value\"><label>Общая площадь <span ng-if=\"!slider.disabled(filter.area)\">{{filter.area}}</span> <span ng-if=\"slider.disabled(filter.area)\">[Все]</span></label><md-slider aria-label=\"area\" ng-model=\"filter.area\" ng-disabled=\"slider.disabled(counts.max_area)\" step=\"1\" min=\"0\" max=\"{{counts.max_area}}\"></md-slider></md-input-container></md-card-content></md-card></div><div flex-gt-md=\"70\" flex-gt-xs=\"100\" flex-md=\"100\" layout=\"column\" ng-show=\"!details.isShow()\"><md-card flex=\"\"><md-card-title><md-card-title-text><span class=\"md-headline\"><md-icon md-font-icon=\"mdi-map\"></md-icon>Объявления на карте</span></md-card-title-text></md-card-title><md-card-content><ui-gmap-google-map center=\"map.center\" zoom=\"map.zoom\" control=\"map.control\" options=\"map.options\"><ui-gmap-markers models=\"markers\" coords=\"\'self\'\" fit=\"true\" type=\"\'cluster\'\" ng-if=\"markers\" typeoptions=\"map.options.cluster\" options=\"map.options.marker\"></ui-gmap-markers><ui-gmap-map-control template=\"/View/Widgets/Map/Control/Draft.html\" position=\"LEFT_TOP\" index=\"-1\"></ui-gmap-map-control><ui-gmap-map-control template=\"/View/Widgets/Map/Control/Zoom.html\" position=\"LEFT_CENTER\" index=\"-1\"></ui-gmap-map-control><ui-gmap-layer type=\"DrawingManager\" namespace=\"drawing\" oncreated=\"map.layers.polygon.init(geometry)\" show=\"\'true\'\"></ui-gmap-layer></ui-gmap-google-map></md-card-content></md-card></div></div>");
$templateCache.put("/View/Widgets/User/Auth.html","<md-input-container><label>Номер телефона</label> <input ng-model=\"user.phone\" mask=\"+7(999)999-9999\"></md-input-container>");
$templateCache.put("/View/Pages/Offer/Add/address.html","<md-grid-list md-cols=\"3\" md-row-height=\"5:1\" md-gutter=\"15px\"><md-grid-tile md-colspan-gt-sm=\"1\" md-rowspan-gt-sm=\"1\"><md-select flex=\"\" ng-disabled=\"geo.location.switch\" placeholder=\"Выберите регион\" ng-model=\"geo.address.region\" md-on-open=\"geo.regions.get()\" ng-change=\"geo.regions.set()\"><md-option ng-value=\"region._id\" ng-repeat=\"region in geo.regions.data\">{{region.name}}</md-option></md-select></md-grid-tile><md-grid-tile md-colspan-gt-sm=\"1\" md-rowspan-gt-sm=\"1\"><md-select flex=\"\" ng-disabled=\"geo.location.switch\" ng-show=\"geo.address.region\" placeholder=\"Выберите город\" ng-model=\"geo.address.city\" md-on-open=\"geo.cities.get()\" ng-change=\"geo.cities.set()\"><md-option ng-value=\"city._id\" ng-repeat=\"city in geo.cities.data[geo.address.region]\">{{city.name}}</md-option></md-select></md-grid-tile><md-grid-tile md-colspan-gt-sm=\"1\" md-rowspan-gt-sm=\"4\"><ui-gmap-google-map flex=\"\" layout-align=\"center center\" refresh=\"true\" center=\"geo.map.center\" zoom=\"geo.map.zoom\" control=\"geo.map.control\"><ui-gmap-marker coords=\"geo.marker.coords\" options=\"geo.marker.options\" events=\"geo.marker.events\" idkey=\"geo.marker.id\"></ui-gmap-marker></ui-gmap-google-map></md-grid-tile><md-grid-tile md-colspan-gt-sm=\"2\" md-rowspan-gt-sm=\"1\"><md-autocomplete flex=\"\" md-no-cache=\"true\" ng-disabled=\"geo.location.switch\" ng-show=\"geo.address.city && geo.address.region\" md-search-text=\"geo.addresses.search_text\" md-selected-item-change=\"geo.addresses.set(item, true)\" md-items=\"item in geo.addresses.get(geo.addresses.search_text)\" md-item-text=\"item.formatted\" md-min-length=\"2\" placeholder=\"Введите улицу и номер дома\"><md-item-template><span md-highlight-text=\"geo.addresses.search_text\" md-highlight-flags=\"^i\">{{item.formatted}}</span></md-item-template><md-not-found>По запросу \"{{geo.addresses.search_text}}\" ничего не найдено.</md-not-found></md-autocomplete></md-grid-tile><md-grid-tile md-colspan-gt-sm=\"2\" md-rowspan-gt-sm=\"1\"><md-switch flex=\"\" layout-align=\"left center\" class=\"md-primary\" md-no-ink=\"\" aria-label=\"Автоопределение\" ng-change=\"geo.location.get()\" ng-show=\"geo.address.city && geo.address.region\" ng-disabled=\"geo.location.is_supported() ? false : true\" ng-model=\"geo.location.switch\">Определить местоположение</md-switch></md-grid-tile></md-grid-list>");
$templateCache.put("/View/Pages/Offer/Add/index.html","<md-card><md-card-title><md-card-title-text><span class=\"md-headline\"><md-icon ng-show=\"step.data.icon\" md-font-icon=\"{{step.data.icon}}\"></md-icon>{{step.data.title}}</span> <span class=\"md-subhead\">{{step.data.description}}</span></md-card-title-text></md-card-title><md-card-content><md-radio-group ng-model=\"offer.type\" ng-hide=\"step.data\" layout=\"row\"><md-radio-button value=\"{{type._id}}\" ng-click=\"step.next(type._id)\" ng-repeat=\"type in types\">{{type.name}}</md-radio-button></md-radio-group><div ng-include=\"\" ng-show=\"step.data.template\" src=\"step.data.template\"></div></md-card-content></md-card><md-button class=\"md-raised md-primary\" ng-click=\"step.prev()\" ng-hide=\"step.first()\">Prev</md-button><md-button class=\"md-raised md-primary\" ng-click=\"step.next()\" ng-hide=\"step.last()\">Next</md-button><md-button class=\"md-raised md-primary\" ng-show=\"step.last() && step.current\">Submit</md-button><md-card><md-card-content>test - {{offer}}</md-card-content></md-card>");
$templateCache.put("/View/Widgets/Map/Control/Draft.html","<div><div><md-button class=\"md-raised md-primary md-mini\" ng-click=\"$parent.$parent.map.control.drawing.trigger()\" ng-show=\"$parent.$parent.map.control.drawing.is.empty()\" aria-label=\"Выделить область\"><md-icon md-font-icon=\"mdi-lead-pencil\"></md-icon></md-button><md-button class=\"md-raised md-primary md-mini\" ng-click=\"$parent.$parent.map.control.drawing.remove()\" ng-show=\"!$parent.$parent.map.control.drawing.is.empty()\" aria-label=\"Удалить область\"><md-icon md-font-icon=\"mdi-delete\"></md-icon></md-button></div><div>{{parent.$parent}}</div><div layout=\"row\" layout-align=\"center center\"><md-progress-circular md-diameter=\"20\" md-mode=\"indeterminate\" ng-if=\"!$parent.$parent.markers\"></md-progress-circular></div></div>");
$templateCache.put("/View/Widgets/Map/Control/Zoom.html","<div><div><md-button class=\"md-raised md-mini\" ng-click=\"$parent.$parent.map.control.zoom.in()\" aria-label=\"+\"><md-icon md-font-icon=\"mdi-plus\"></md-icon></md-button></div><div><md-button class=\"md-raised md-mini\" ng-click=\"$parent.$parent.map.control.zoom.out()\" aria-label=\"-\"><md-icon md-font-icon=\"mdi-minus\"></md-icon></md-button></div></div>");}]);
app.factory('$Dictionary', [
  '$rootScope', '$q', '$API',
  function($rootScope, $q, $API){
    return function(){

      var $api = $API.all('dictionaries');

      var $dictionary = {
        list:function(filter, sort){
          var deferred = $q.defer();
          $api.customGET().then(function(items){
            items = filter ? _.filter(items, filter) : items;
            items = sort ? _.sortBy(items, sort) : items;
            deferred.resolve(items);
          });
          return deferred.promise;
        },
        code:function(code){
          return $api.all('code')
              .customGET(code);
        },
        alias:function(item, num){
          if(item){
            if(_.isArray(item.aliases) && item.aliases[num]){
              return item.aliases[num].name;
            }
            return item.name
          }
          return null;
        }
      };

      return $dictionary;
    }
  }
]);
app.factory('$Location', [
  '$rootScope', '$q', '$API',
  function($rootScope, $q, $API){
    return function(){

      var $api = $API.all('locations');

      var $location = {
        current:function() {
          return $api.all('current').customGET();
        },
        regions:function(){
          return $api.all('regions').getList();
        },
        cities:function(region_id){
          return $api.all('cities').all(region_id).getList();
        }
      };

      return $location;
    }
  }
]);
app.service('$Offer', [
  '$rootScope', '$API',
  function($rootScope, $API){
    return function(){

      var $api = $API.all('offers');

      var $offer = {
        group:function(type_id){
          return $api.all('group').customGET(type_id);
        },
        view:function(offer_id){
          return $api.all('view').customGET(offer_id);
        },
        counts:function(params){
          return $api.all('counts').customGET(null, params);
        },
        index:function(params){
          return $api.customGET(null, params);
        },
        points:function(params){
          return $api.all('points').customGET(null, params);
        }
      };
      return $offer;
    }
  }
]);
app.factory('$Photo', [
  '$rootScope', '$q', '$API',
  function($rootScope, $q, $API){
    return function(){

      var $api = $API.all('photos');
      var $photo = {
        resize:function(id, width, height){
          return $api.one('resize', id).customPUT({
            width:width,
            height:height
          });
        }
      };

      return $photo;
    }
  }
]);
app.factory('$User', [
  '$rootScope', '$API', '$Dialog',
  function($rootScope, $API, $Dialog){
    return function(){

      var $api = $API.all('users');

      var $user = {
        current:function(){
          return $api.customGET();
        },
        check:function(phone){
          return $api.one('check', phone).get();
        },
        dialog:function(){
          $Dialog({
            open:{
              width:1000
            },
            title:'Авторизация',
            include:'/View/Widgets/User/Auth.html',
            controller:function($scope){
              $scope.user = {};

              $scope.$watchCollection(function(){
                return [
                  $scope.user.phone
                ];
              }, function(){
                if($scope.user.phone && $scope.user.phone.length >= 15){
                  $user.check($scope.user.phone).then(function(check){
                    void 0;
                  });
                }
              });
            }
          });
        }
      };

      return $user;
    }
  }
]);
app.directive('paginate', function(){
  return {
    restrict: 'EA',
    scope:{
      model:'<',
      filter:'=',
      length:'=',
      align:'@'
    },
    templateUrl:'/View/Widgets/Paginate.html',
    controller:function($scope) {
      $scope.model = $scope.model || { paging:{} };

      $scope.goPage = function(page){
        $scope.filter.page = page;
        $scope.model.paging.current = page;
      };

      $scope.goFirst = function(){
        $scope.goPage(1);
      };

      $scope.goLast = function(){
        $scope.goPage($scope.model.paging.count);
      };

      $scope.goPrev = function(){
        $scope.goPage($scope.model.paging.current - 1);
      };

      $scope.goNext = function(){
        $scope.goPage($scope.model.paging.current + 1);
      };

      $scope.showFirst = function(){
        return $scope.model.paging.current <= $scope.length / 2 + 1
      };

      $scope.showLast = function(){
        return $scope.model.paging.current >= $scope.model.paging.count - $scope.length / 2
      };

      $scope.showNext = function(){
        return $scope.model.paging.next;
      };

      $scope.showPrev = function(){
        return $scope.model.paging.prev;
      };

      $scope.showPaginate = function(){
        return $scope.model.paging.count >= 1;
      };

      $scope.$watchCollection(function(){
        return [
          $scope.length,
          $scope.filter.page,
          $scope.model.length
        ];
      }, function() {

        $scope.length = Number($scope.length) || 10;
        $scope.pages = [];

        var min = $scope.model.paging.current - Math.ceil($scope.length / 2),
            max = $scope.model.paging.current + Math.ceil($scope.length / 2);

        if(min <= 0 || !min){
          min = 0;
          max = $scope.length;
        }

        if(max > $scope.model.paging.count + 1 && min >= 0){
          min = min - max + $scope.model.paging.count + 1;
          max = $scope.model.paging.count + 1;
        }

        _.forEach(new Array($scope.model.paging.count), function(value, key){
          if($scope.pages.length < $scope.length && key >= min && key < max){
            $scope.pages.push(key + 1);
          }
        });
      });
    }
  };
});
app.directive('photo', function(){
  return {
    restrict: 'EA',
    scope:{
      src:'=',
      width:'=',
      height:'=',
      alt:'&'
    },
    template:'<img ng-src="{{host}}{{photo.url}}" ng-if="showImg()" />',
    controller:[
      '$scope', '$Photo',
      function($scope, $Photo){

        $scope.model = new $Photo();
        $scope.host = config.api ? config.api.setBaseUrl : null;

        $scope.findMain = function(){
          return _.find($scope.src, {
            is_main:true
          }) || _.head($scope.src);
        };

        $scope.findSize = function() {
          return $scope.main ? _.find($scope.main.sizes, {
            width:$scope.width,
            height:$scope.height
          }) : null;
        };

        $scope.showImg = function(){
          return $scope.photo && $scope.photo.url;
        };

        $scope.$watchCollection('[src, width, height]', function(){
          if(!_.isUndefined($scope.src)){
            if(!_.isArray($scope.src)){
              $scope.main = $scope.src;
            } else {
              $scope.main = $scope.findMain();
            }
            $scope.photo = $scope.findSize();
            if($scope.main && !$scope.photo){
              $scope.model.resize($scope.main.id, $scope.width, $scope.height)
                .then(function(photo){
                  $scope.photo = photo;
                });
            }
          }
        });
      }
    ]
  };
});
app.directive('preload', function(){
  return {
    restrict: 'EA',
    scope:{
      model:'=',
      align:'@',
      diameter:'@'
    },
    template:[
      '<div layout="row" class="material-preload" layout-align="{{align}}" ng-if="show">',
        '<md-progress-circular md-diameter="{{diameter}}" md-mode="indeterminate" class=""></md-progress-circular>',
      '</div>'
    ].join(''),
    controller:function($scope) {
      $scope.show = true;
      $scope.$watch('model', function(){
        $scope.show = _.isUndefined($scope.model);
      });
    }
  };
});
app.controller('IndexCtrl', [
  '$scope', '$Offer',
  function($scope, $Offer){

    $scope.offer = new $Offer();

    $scope.change_type = function(type_id){
      delete $scope.offer_groups;
      $scope.offer.group(type_id).then(function(groups){
        $scope.offer_groups = groups;
      });
    };

    $scope.$DY.list({ code: 'offer_type' }).then(function(types){
      $scope.offer_types = types;
    });

    $scope.change_type();
    $scope.offer_type_id = 0;

  }
]);
app.controller('MainCtrl', [
  '$scope', '$User', '$Dictionary', '$mdMedia',
  function($scope, $User, $Dictionary, $mdMedia){

    $scope.$DY = new $Dictionary();
    $scope.$mdMedia = $mdMedia;
    $scope.$Moment = moment;

   

    $scope.menu_groups = [
      {
        id: 1,
        name: 'Меню',
        menu:[
          {
            id: 1,
            name: 'На Главную',
            description:'',
            keywords:'',
            href:'main.index',
            access: {
              id: 1,
              code: 'Guest'
            }
          },
          {
            id: 2,
            name: 'Все Объявления',
            description:'',
            keywords:'',
            href:'main.offers',
            access: {
              id: 1,
              code: 'Guest'
            }
          }
        ]
      },
      {
        id:2,
        name:'Личный кабинет',
        menu:[
          {
            id:3,
            name:'Мои объявления',
            description:'',
            keywords:'',
            href:'main.cabinet.offers',
            access:{
              id:2,
              code:'User'
            }
          }
        ]
      },
      {
        id:3,
        name:'Управление сайтом',
        menu:[
          {
            id:4,
            name:'Все пользователи',
            description:'',
            keywords:'',
            href:'main.admin.users',
            access:{
              id:3,
              code:'Moderator'
            }
          }
        ]

      }
    ];
  }
]);
app.service('$API', [
  'Restangular', '$Storage',
  function(Restangular, $Storage){

    var $api = Restangular.withConfig(function(setting){
      _.forEach(config.api, function(value, key) {
        setting[key](value);
      });
    });

    _.assignIn($api, {
      _params:function(params, name){
        var result = {};
        if(!_.isObject(params)){
          result[name] = params;
        } else {
          _.forEach(params, function(value, key){
            key = name ? name+'['+key+']' : key;
            if(_.isArray(value)){
              _.forEach(value, function(v, i){
                _.assignIn(result, $api._params(v, key+'['+i+']'));
              });
            } else if(_.isObject(value)){
              _.assignIn(result, $api._params(value, key));
            } else {
              result[key] = value;
            }
          });
        }
        return result;
      },
      _request:function(params) {
        var result = [];
        if (_.isObject(params)) {
          _.forEach(this._params(params), function (value, key) {
            result.push([key, encodeURIComponent(value)].join('='));
          });
        }
        return result.join('&');
      },
      _headers:function(method, headers){
        if(_.indexOf(['post', 'put', 'delete', 'options'], method) >= 0){
          _.assignIn(headers, {
            'Content-Type':'application/x-www-form-urlencoded'
          });
        }
        if(_.isString($Storage.get('accessToken'))){
          _.assignIn(headers, {
            'Access-Token':$Storage.get('accessToken')
          })
        }
        if(_.isString($Storage.get('userToken'))){
          _.assignIn(headers, {
            'Authorization':'Bearer ' + $Storage.get('userToken')
          })
        }
        return headers;
      },
      _response:function(response){
        if(response){
          return _.assignIn(response.data, {
            paging:response.paging
          });
        }
      }
    });

    $api.setErrorInterceptor(function(response, deferred, responseHandler){
      if(response.data && response.data.status){
        if(response.data.status === true){
          responseHandler(response);
          return false;
        } else {
          void 0;
        }
      }
    }).addResponseInterceptor(function(response){
      return $api._response(response);
    }).addFullRequestInterceptor(function(element, method, route, url, headers, params){
      return {
        headers:$api._headers(method, headers),
        element:$api._request(element),
        params:$api._params(params)
      };
    });

    return $api;
  }
]);
app.service('$Dialog', [
  '$mdDialog',
  function($mdDialog){
    return function(options){
      return $mdDialog.show({
        openFrom:_.assignIn(options.open, {
          top:-50,
          width:30,
          height:80
        }),
        closeTo:_.assignIn(options.close, {
          left: 1500
        }),
        templateUrl:'View/Widgets/Dialog.html',
        clickOutsideToClose:true,
        controller:function($scope, $mdDialog) {
          $scope.hide = function() {
            $mdDialog.hide();
          };

          if(_.isString(options.include)){
            $scope.include = options.include;
          }

          if(_.isString(options.title)){
            $scope.title = options.title;
          }

          if(_.isString(options.body)){
            $scope.body = options.body;
          }

          if(_.isFunction(options.controller)){
            options.controller($scope, $mdDialog);
          }
        }
      });
    }
  }
]);
app.service('$Map', [
  '$rootScope', 'uiGmapGoogleMapApi', '$q',
  function($rootScope, uiGmapGoogleMapApi, $q){

    var $map = {
      zoom:8,
      center:{
        latitude:55,
        longitude:37
      },
      options:{
        scrollwheel:true,
        disableDefaultUI:true,
        marker:{
          icon:'/Assets/Images/pointmap.png'
        },
        polygon:{
          strokeColor:"#3B3B3B",
          strokeOpacity:0.7,
          strokeWeight:2,
          clickable:false,
          editable:true,
          draggable:true,
          fillOpacity:0,
          zIndex:1
        },
        cluster: {
          minimumClusterSize:3,
          zoomOnClick:true,
          styles:[
            {
              url:"/Assets/Images/boxmap30.png",
              width:30,
              height:30,
              textColor:'black',
              textSize:10,
              fontFamily:'Roboto'
            },
            {
              url:"/Assets/Images/boxmap40.png",
              width:40,
              height:40,
              textColor:'black',
              textSize:11,
              fontFamily:'Roboto'
            },
            {
              url:"/Assets/Images/boxmap50.png",
              width:50,
              height:50,
              textColor:'black',
              textSize:12,
              fontFamily:'Roboto'
            },
            {
              url:"/Assets/Images/boxmap70.png",
              width:70,
              height:70,
              textColor:'black',
              textSize:12,
              fontFamily:'Roboto'
            }
          ],
          averageCenter:true,
          clusterClass:'cluster-icon'
        }
      },
      control:{
        zoom:{
          get:function(){
            return $map.control.getGMap().getZoom();
          },
          set:function(zoom){
            $map.control.getGMap().setZoom(zoom);
          },
          in:function(){
            this.set(this.get() + 1);
          },
          out:function(){
            this.set(this.get() - 1);
          },
          array:function(array){
            var bounds = new google.maps.LatLngBounds();
            array.forEach(function (coordinates) {
              bounds.extend(coordinates);
            });
           
            void 0;
           
          }
        },
        center:{
          get:function(){
            $map.control.getGMap().getCenter();
          },
          set:function(center){
            $map.control.getGMap().setCenter(center);
          },
          array:function(array){
            var bounds = new google.maps.LatLngBounds();
            array.forEach(function (coordinates) {
              bounds.extend(coordinates);
            });
            this.set(bounds.getCenter());
          }
        },
        drawing:{
          draft:false,
          trigger:function() {
            this.draft = !this.draft;
          },
          is:{
            draft:function(){
              return $map.control.drawing.draft;
            },
            empty:function(){
              return $map.control.drawing.object.data.length === 0;
            }
          },
          remove:function(){
            $map.control.listener.remove();
            this.object.remove();
            this.draft = false;
          },
          object:{
            data:[],
            set:function(object){
              return this.data.push(object);
            },
            remove:function(id){
              if(_.isInteger(id)){
                this.data[id - 1].setMap(null);
                delete this.data[id - 1];
              } else {
                _.forEach(this.data, function(data){
                  data.setMap(null);
                });
                this.data = [];
              }
            }
          }
        },
        ready:function(){
          var deferred = $q.defer();
          uiGmapGoogleMapApi.then(function(maps) {
            deferred.resolve(maps);
          });
          return deferred.promise;
        },
        refresh:function(){
          google.maps.event.trigger($map.control.getGMap(), 'resize');
        },
        listener:{
          data:[],
          set:function(object, event, geometry){
            return this.data.push(google.maps.event.addListener(object, event, geometry));
          },
          remove:function(id){
            if(_.isInteger(id)){
              this.data[id - 1].remove();
              delete this.data[id - 1];
            } else {
              _.forEach(this.data, function(data){
                data.remove();
              });
              this.data = [];
            }
          }
        },
        geoJSON:{
          encode:function(geometry){
            if(_.isArray(geometry.coordinates)){
              var array = [];
              _.forEach(geometry.coordinates, function(coordinates){
                _.forEach(coordinates, function(coordinate){
                  array.push(new google.maps.LatLng(coordinate[1], coordinate[0]));
                });
              });
              return array;
            }
            return null;
          }
        },
        geometry:{
          decode:function(string){
            return google.maps.geometry.encoding.decodePath(string);
          },
          encode:function(array){
            return google.maps.geometry.encoding.encodePath(array);
          }
        }
      },
      layers:{
        polygon:{
          listeners:function(polygon, geometry){
            $map.control.listener.set(polygon.getPath(), 'set_at', function() {
              geometry.set(polygon);
            });
            $map.control.listener.set(polygon.getPath(), 'insert_at', function() {
              geometry.set(polygon);
            });
            $map.control.listener.set(polygon.getPath(), 'remove_at', function() {
              geometry.set(polygon);
            });
            $map.control.listener.set(polygon.getPath(), 'drag', function() {
              geometry.set(polygon);
            });
            $map.control.drawing.object.set(polygon);
            $map.control.center.array(polygon.getPath());
            $map.control.zoom.array(polygon.getPath());
            geometry.set(polygon);
          },
          watcher:function(layer, geometry){
            $rootScope.$watch(function(){
              return $map.control.drawing.is.draft();
            }, function(){
              if(!$map.control.drawing.is.draft()){
                layer.setDrawingMode(null);
              } else {
                layer.setDrawingMode('polygon');
                $map.control.listener.set(layer, 'polygoncomplete', function(polygon) {
                  $map.layers.polygon.listeners(polygon, geometry);
                  $map.control.drawing.trigger();
                });
              }
            });
            $rootScope.$watch(function(){
              return geometry.get();
            }, function(){
              $map.control.drawing.object.remove();
              if(geometry.get()){
                var polygon = new google.maps.Polygon(_.assignIn($map.options.polygon, {
                  editable:false
                }));
                polygon.setPath($map.control.geometry.decode(geometry.get()));
                $map.layers.polygon.listeners(polygon, geometry);
                polygon.setMap($map.control.getGMap());
              }
            });
            $rootScope.$watch(function(){
              return $map.control.drawing.is.empty();
            }, function(){
              if($map.control.drawing.is.empty()){
                geometry.set(null);
              }
            });
          },
          init:function(geometry){
            return function(layer){
              $map.layers.polygon.watcher(layer, geometry);
              $map.control.refresh();
              layer.setOptions({
                drawingControl:false,
                polygonOptions:$map.options.polygon
              });
            };
          }
        }
      }
    };

    return $map;
  }
]);
app.service('$Storage', [
  '$localStorage',
  function($localStorage){

    var $storage = {
      set:function(name, value){
        return $localStorage[name] = value;
      },
      get:function(name){
        return $localStorage[name];
      },
      remove:function(name) {
        return delete $localStorage[name];
      }
    };

    return $storage;
  }
]);
app.directive('locationSelect', function(){
  return {
    restrict: 'EA',
    scope:{
      geometry:'=',
      region:'=',
      city:'=',
      empty:'@'
    },
    templateUrl:'/View/Widgets/Location/Select.html',
    controller:[
      '$scope', '$Location', '$Map',
      function($scope, $Location, $Map){

        var location = new $Location(),
          map = $Map;

        $scope.current = {
          status:0,
          trigger:function(){
            switch (this.status){
              case 0:
                this.status = 2;
                location.current().then(function(city){
                  $scope.current.status = 1;
                  $scope.region = city.region.id;
                  $scope.city = city.id;
                });
                break;
              case 1:
                this.status = 0;
                delete $scope.regions.data;
                delete $scope.cities.data;
                delete $scope.geometry;
                delete $scope.region;
                delete $scope.city;
                break;
            }
          },
          is:function(status){
            this.status = $scope.region ? 1 : this.status;
            return this.status === status;
          }
        };

        $scope.isEmpty = function(){
          return String($scope.empty).length > 0;
        };

        $scope.regions = {
          get:function(){
            delete $scope.regions.data;
            return location.regions().then(function(regions){
              $scope.regions.data = regions;
              $scope.regions.geometry();
            });
          },
          geometry:function(region){
            if(!$scope.city || !$scope.geometry){
              region = region || _.find(this.data, {
                  id:$scope.region
                });
              if(region && region.boundary){
                $scope.geometry = map.control.geometry.encode(
                  map.control.geoJSON.encode(
                    region.boundary.geometry
                  )
                );
              } else {
                delete $scope.geometry;
              }
            }
          },
          set:function(){
            delete $scope.city;
            this.geometry();
          }
        };

        $scope.cities = {
          get:function(){
            delete $scope.cities.data;
            return location.cities($scope.region).then(function(cities){
              $scope.cities.data = cities;
              $scope.cities.geometry();
            });
          },
          geometry:function(city){
            city = city || _.find(this.data, {
                id:$scope.city
              });
            if(city && city.boundary){
              $scope.geometry = map.control.geometry.encode(
                map.control.geoJSON.encode(
                  city.boundary.geometry
                )
              );
            } else {
              delete $scope.geometry;
              $scope.regions.geometry();
            }
          },
          set:function(){
            this.geometry();
          }
        };

        $scope.$watchCollection(function(){
          return [
            $scope.region
          ];
        }, function(){
          if($scope.region){
            $scope.regions.get();
            $scope.cities.get();
          }
        });
      }
    ]
  };
});





  app.directive('offerFilter', function(){
  return {
    restrict: 'EA',
    scope:{
      _filter:'=filter',
    },
    templateUrl:'/View/Widgets/Offer/Filter.html',
    controller:[
      '$scope', '$Offer', '$Dictionary', '$Map', '$state',
      function($scope, $Offer, $Dictionary, $Map, $state){

        var offer = new $Offer(),
            dictionary = new $Dictionary();

        $scope.map = $Map;
        $scope.filter = _.clone($scope._filter || {
          tags:[],
          rooms:[]
        });

        $scope.submit = function(){
          $scope.filter.page = 1;
          $state.go('main.offers', $scope.filter);
        };

        $scope.room = {
          checked:function(id){
            if(!_.isArray($scope.filter.rooms)){
              $scope.filter.rooms = [$scope.filter.rooms];
            }
            if(id) return _.indexOf($scope.filter.rooms, id) >= 0;
            return $scope.filter.rooms.length === 0;
          },
          toggle:function(id, length){
            if(!id || length <= $scope.filter.rooms.length + 1) {
              $scope.filter.rooms = [];
            } else {
              if(!this.checked(id)){
                $scope.filter.rooms.push(id);
              } else {
                _.remove($scope.filter.rooms, function(r_id){
                  return id === r_id;
                });
              }
            }
          }
        };

        $scope.geometry = {
          set:function(geometry){
            if(!geometry){
              $scope.filter.geometry = null;
            } else {
              $scope.filter.geometry = $scope.map.control.geometry.encode(
                geometry.getPath()
              );
            }
          },
          get:function(){
            return $scope.filter.geometry;
          }
        };

        $scope.slider = {
          disabled:function(num){
            return !_.isNumber(num) || num <= 0;
          }
        };

        $scope.details = {
          show:false,
          trigger:function(){
            this.show = !this.show;
          },
          isShow:function(){
            return this.show;
          }
        };

        _.forEach({
          types:{
            code:'offer_type'
          },
          periods:{
            code:'offer_period'
          },
          rooms:function(item){
            return item.code === 'offer_rooms' && item.aliases[3];
          }
        }, function(filter, key){
          dictionary.list(filter, function(o){
            return dictionary.alias(o, 3);
          }).then(function(items){
            $scope[key] = items;
          });
        });

        $scope.$watchCollection(function(){
          return [
            $scope.filter.region, $scope.filter.city,
            $scope.filter.period, $scope.filter.type,
            $scope.filter.geometry,
            $scope.filter.rooms.length,
            $scope.filter.tags.length
          ];
        }, function(){
          delete $scope.markers;
          offer.counts($scope.filter).then(function(counts){
            $scope.counts = counts;
          });
          offer.points($scope.filter).then(function(points){
            $scope.markers = [];
            _.forEach(points, function(point){
              $scope.markers.push({
                id:point.id,
                latitude:point.geometry.coordinates[1],
                longitude:point.geometry.coordinates[0]
              });
            });
          });
        });

      }
    ]
  };
});
app.controller('OfferAddCtrl', [
  '$scope', '$GEO',
  function($scope, $GEO){
    var steps = {
      address:function(){
        var geo = new $GEO();

        $scope.$User.current().then(function(user){
          if(user && user.geo) geo.auto({
            region:user.geo.region._id,
            city:user.geo.city._id
          });
        });

        geo.then(function(address){
          $scope.offer.address = address;
        });

        $scope.geo = geo;
      }
    };

    $scope.offer = {};

    $scope.$DY.code('offer_type').then(function(types){
      $scope.types = types;
    });

    


  }
]);
app.controller('OfferIndexCtrl', [
  '$scope', '$Offer', '$state', '$Dialog',
  function($scope, $Offer, $state, $Dialog){

    $scope.filter = {
      region:$state.params.region,
      city:$state.params.city,
      page:$state.params.page,
      period:$state.params.period,
      tags:$state.params.tags,
      rooms:$state.params.rooms,
      type:$state.params.type,
      price:$state.params.price,
      floor:$state.params.floor,
      area:$state.params.area,
      geometry:$state.params.geometry
    };

    $scope.$watchCollection(function(){
      return [$scope.filter.page];
    }, function(){
      $state.go('main.offers', $scope.filter);
    });

    var offer = new $Offer();

    offer.index($scope.filter).then(function(offers){
      $scope.offers = offers;
    });

  }
]);
app.controller('OfferViewCtrl', [
  '$scope', '$Offer', '$state',
  function($scope, $Offer, $state){

    $scope.offer = {};

    var offer = new $Offer();

    offer.one($state.params.id).then(function(offer){
      $scope.offer = offer;
    });
  }
]);